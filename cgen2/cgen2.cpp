/*==============================================================================
   File: cgen2.cpp

   Description:
		PIFAGOR control graph generator

   Credits: AUTHORS

   License: GNU General Public License Version 3 or later
            http://www.gnu.org/licenses/gpl.html
==============================================================================*/

#include <cstdlib>
#include <cstring>
#include <wchar.h>
#include <locale.h>

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
    #include <direct.h>
#endif

using namespace std;
			   
#include "../repos2/repos2.h"
#include "../rigparser/rigparser.h"
#include "../cg/cg.h"
#include "../cg2dot/cg2dot.h"

bool SaveCgDot(Cg& cg, QString fileName);

#define CGEN_SUCCESS	0
#define ARGNUM_ERROR	1
#define INFILE_ERROR	2

/*============================================
    Input arguments:
        <key> <input_file> <output_file>
        <nkey> <function_name>
    where:
        <key> is
            -t for text output format
            -d for dot output format
        <nkey> is
            -nt for text output format
            -nd for dot output format
============================================*/

void cgen_arg_error()
{
	printf("\nWrong input format. Correct formats:\n");
	printf("\t<key> <input_file> <output_file>\n");
	printf("\t<nkey> <function_name>\n");
	printf("where <key> is\n");
    printf("\t-t for text output format\n");
	printf("\t-d for dot output format\n");		
	printf("where <nkey> is\n");
    printf("\t-nt for text output format\n");
    printf("\t-nd for dot output format\n");
    printf("\t-na for text&dot output format\n");
}

int main(int argc, char *argv[])
{
    setlocale(LC_CTYPE,"en_US.UTF-8");
	if(argc<3)
	{
		cgen_arg_error();
		return ARGNUM_ERROR;
	}

    Rig rig;
    Cg cg;
    repos rep;
    RigParser *parser;

    bool result;
    if(argv[1][1]=='n')         //-nt, -nd или -na
    {
        parser=new RigParser(&rig,QString(rep.getIgTxtName(argv[2])));

        result=parser->Start();
        if(!result)
        {
            printf("Input file error\n");
            return INFILE_ERROR;
        }
        cg.Transform(parser->GetRig());

        //making output
        if(!strcmp(argv[1],"-nt"))    //text
            cg.SaveText(rep.prepCgTxt(cg.GetCharName()));
        if(!strcmp(argv[1],"-nd"))    //dot
            SaveCgDot(cg, rep.prepCgDot(cg.GetCharName()));
        if(!strcmp(argv[1],"-na"))    //text&dot
        {
             cg.SaveText(rep.prepCgTxt(cg.GetCharName()));
             SaveCgDot(cg, rep.prepCgDot(cg.GetCharName()));
        }
    }
    else                        //-t или -d
    {
        parser=new RigParser(&rig,QString(argv[2]));
        result=parser->Start();
        if(!result)
        {
            printf("Input file error\n");
            return INFILE_ERROR;
        }
        cg.Transform(parser->GetRig());
        rep.dirPrepare(argv[3]);
        //making output
        if(!strcmp(argv[1],"-t"))    //text
            cg.SaveText(argv[3]);
        if(!strcmp(argv[1],"-d"))    //dot
            SaveCgDot(cg,argv[3]);
    }
    printf("CG generation completed successfully\n");
    return CGEN_SUCCESS;
}

