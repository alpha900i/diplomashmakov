/*==============================================================================
   File: trans2.cpp

   Description:
       Pifagor RIG translator 2.0

   Credits: AUTHORS

   License: GNU General Public License Version 3 or later
            http://www.gnu.org/licenses/gpl.html
==============================================================================*/

#include <cstdlib>
#include <string.h>
#include <wchar.h>
#include <locale.h>

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#include <direct.h>
#endif

using namespace std;

#include "pfg_parser2.h"
#include "../repos2/repos2.h"
#include "../rig2dot/rig2dot.h"

bool SaveRigDot(Rig& rig, QString fileName);

#define TRANS_SUCCESS    0
#define ARGNUM_ERROR    1
#define INFILE_ERROR    2
#define ERRFILE_ERROR    3
#define DBGFILE_ERROR    4
#define TRANS_ERROR    5


/*========================================================
    Input arguments:
        <key> <input_file> <error_file> <debug_file>
    where:
        <key> is
            -c for constant translation
            -d for dot output format
            -t for text output format
========================================================*/

void trans_arg_error()
{
    printf("\nWrong input format. Correct format:\n");
    printf("\t<key> <input_file> [error_file> [debug_file]]\n");
    printf("where <key> is\n");
    printf("\t-c for constant translation\n");
    printf("\t-d for dot output format\n");
    printf("\t-t for text output format\n");
    printf("\t-a for text&dot output format\n");
}

int main(int argc, char *argv[]) {
  
    #if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
      mkdir("./out");
      mkdir("./out/db");
    #else
      mkdir("./out",0777);
      mkdir("./out/db",0777);
    #endif

    repos rep;
    bool rmode=false;

    setlocale(LC_CTYPE,"en_US.UTF-8");

    FILE* in    = 0; //input
    FILE* err   = 0; //translation errors output
    FILE* dbg   = 0; //debug information

    if (argc < 3)
    {
        trans_arg_error();
        return ARGNUM_ERROR;
    };

    if(!strcmp(argv[1],"-ra") || !strcmp(argv[1],"-rt") || !strcmp(argv[1],"-rd"))
    {
        argv[1][1]=argv[1][2];
        argv[1][2]='\0';
        in=fopen(rep.getCodeName(argv[2]),"r");
        rmode=true;
    }
    else
    {
        in   = fopen(argv[2],"r");
    }
    
    // Если не указаны четвертый (а следовательно и пятый) аргументы, то имена файлов ошибок назначаются по умолчанию.
    // error.txt - файл ошибок по умолчанию.
    // debug.txt - файл отладочной информации по умолчанию
    // Файлы создаются в текущем каталоге программы (пока).
    if(argc < 4) {
      err = fopen("error.txt", "w");
      dbg = fopen("debug.txt", "w");
    } else if(argc < 5) {
      err  = fopen(argv[3],"w");
      dbg = fopen("debug.txt", "w");
    } else {
      err  = fopen(argv[3],"w");
      dbg  = fopen(argv[4],"w");
    }

    //file error handling
    if (in == 0)
    {
        printf("Input file error\n");
        fwprintf(stdout, L"Input file opening error.\n");
        return INFILE_ERROR;
    };
    if (err == 0)
    {
        printf("Errors file error\n");
        fwprintf(stdout, L"Errors file opening error.\n");
        return ERRFILE_ERROR;
    };
    if (dbg == 0)
    {
        printf("Debug file error\n");
        fwprintf(stdout, L"Debug information file opening error.\n");
        return DBGFILE_ERROR;
    };

    //translation
    pfg_scanner my_scaner(in, err, dbg);
    my_scaner.print(dbg);


    pfg_parser my_parser;
    if(strcmp(argv[1],"-c"))
    {
        if (!my_parser.parse(&my_scaner))
        {
            //Translation error handling
            printf("Program error\n");
            fwprintf(err, L"Program error %d %d\n", my_parser.error_begin->pos.GetBeginRow(), my_parser.error_begin->pos.GetBeginCol());          
            return TRANS_ERROR;
        };
    }
    else
    {
        if (!my_parser.parse_const(&my_scaner))
        {
            //Translation error handling
            printf("Constant error\n");
            fwprintf(err, L"Constant error %d %d\n", my_parser.error_begin->pos.GetBeginRow(), my_parser.error_begin->pos.GetBeginCol());
            return TRANS_ERROR;
        };
    }

    fclose(in);

    //making output
    if(!strcmp(argv[1],"-t"))    //text
        for(unsigned long i=0;i<my_parser.funcRigs.size();i++)
            my_parser.funcRigs[i]->SaveText(rep.prepIgTxt(my_parser.funcRigs[i]->GetCharName()));
    if(!strcmp(argv[1],"-d"))    //dot
        for(unsigned long i=0;i<my_parser.funcRigs.size();i++)
            SaveRigDot(*(my_parser.funcRigs[i]),rep.prepIgDot(my_parser.funcRigs[i]->GetCharName()));
    if(!strcmp(argv[1],"-a"))
        for(unsigned long i=0;i<my_parser.funcRigs.size();i++)
        {
            my_parser.funcRigs[i]->SaveText(rep.prepIgTxt(my_parser.funcRigs[i]->GetCharName()));
            SaveRigDot(*(my_parser.funcRigs[i]),rep.prepIgDot(my_parser.funcRigs[i]->GetCharName()));
        }
    if(!strcmp(argv[1],"-c"))    //const
    {
        my_parser.funcRigs[0]->SaveText("arg.rig");
    }
    else
    {
        if(!rmode)
            for(unsigned long i=0;i<my_parser.funcPos.size()-1;i++)
                rep.saveCode(argv[2],my_parser.funcPos[i],my_parser.funcPos[i+1],my_parser.funcRigs[i]->GetCharName());
    }

    fclose(err);
    fclose(dbg);
    printf("Translation completed successfully\n");
    return TRANS_SUCCESS;
}

