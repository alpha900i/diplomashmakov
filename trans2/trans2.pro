CONFIG   += console
TEMPLATE = app

OBJECTS_DIR = ../../_build/obj
DESTDIR  = ../../bin
#MAKEFILE = make_trans

TARGET = trans2

HEADERS += ../repos2/repos2.h \
    pfg_scanner2.h \
    pfg_parser2.h \
    ../rig/rig.h \
    ../rig/extref.h \
    ../rig/actor.h \
    ../rig/value.h \
    ../rig2dot/rig2dot.h

SOURCES += ../repos2/repos2.cpp \
    trans2.cpp \
    pfg_scanner2.cpp \
    pfg_parser2.cpp \
    ../rig/rig.cpp \
    ../rig/extref.cpp \
    ../rig/actor.cpp \
    ../rig/value.cpp \
    ../rig2dot/rig2dot.cpp
