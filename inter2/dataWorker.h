#ifndef DATAWORKER_H
#define DATAWORKER_H
#include "../rig/rig.h"
#include "EventProcessor.h"
#include "alu.h"

#define DW_NOTHING      0
#define DW_DELAYOPEN    1

class EventProcessor;
struct DataWorkerTask
{
    int nodeNum;
    int input;
    DataWorkerTask(){;}
    DataWorkerTask(int newNodeNum, int newInput): nodeNum(newNodeNum),input(newInput){;}
};
struct InterpActorQueues
{
    QQueue<int> argQueue;
    QQueue<int> funcQueue;
};

class DataWorker
{
    EventProcessor *myEp;
    Rig *myRig;
    Value *myArg;
    QQueue<DataWorkerTask> dataTasks;                   //задания для обработчика данных
    QVector<Value*> actorDataLayer;                     //слой данных (акторы)

    QVector<Value*> localConstList;
    QVector<Actor*> actorList;                          //вектор узлов-акторов

    //коллекция указателей на спецоперации
    SpecValue** specList;
    InterpActorQueues* interpActorQueuesList;
public:
    Value *rezult;                                      //результат
    DataWorker() {;};
    DataWorker(EventProcessor* newEp, Rig* newRig, Value* newArg=NULL);
    void CopyLocalConstList();

    void Init();
    void InitActorDataLayer();                          //формирование слоя данных для акторов
    void SetArgumentIntoDataLayer();
    void SendExtraSignals(ParListValue* constant, int nodeNum);
    void PrintActorDataLayer();                         //печать слоя данных для акторов
    void FormSpecList();


    void Work();
    void WorkOne();
    void AddTask(int taskNode, int taskInput);
    bool ExecuteTask(DataWorkerTask curTask);
    void ExecuteArgument();
    void ExecuteDataList(DataWorkerTask curTask);
    void ExecuteParList(DataWorkerTask curTask);
    void ExecuteAsyncList(DataWorkerTask curTask);
    void ExecuteReturn(DataWorkerTask curTask);
    void ExecuteInter(DataWorkerTask curTask);
    void ProcessSinglePair(int argNum, int funcNum, int nodeNum);

    void ReserveInterpretAnswer(int nodeNum, int answerLen);
    void FillInterpretQueues(DataWorkerTask task);
    void FillInterpretQueue(Value *obj, QQueue<int> *queue);

    Value* GetFromLocation(MemLocation curLocation);    //получаем блок данных из нужного слоя
    Value* MakeDataList(int index);                     //собираем уже готовый список данных
    Value* AddToParList(DataWorkerTask currentTask);
    void SetData(int nodeNum, Value *newData);
    void SetData(int nodeNum, Value *newData, int newDataIndex);
    SpecValue* GetSpecOp(int x);
    int GetMemLocationMultiplicity(MemLocation curMemLocation);
    int GetActorMultiplicity(int actorIndex);
    int GetLocalMultiplicity(int localIndex);
    int GetLinksCount(int nodeIndex);

    void CallExtern(int externId, int externOwnerID, Value *arg, int nodeNum, int posIndex);

    Value* GetDelayResult(Value* arg);
};

#endif // DATAWORKER_H
