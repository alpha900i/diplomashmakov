/*==============================================================================
   File: trans2.cpp

   Description:
       Pifagor RIG translator 2.0

   Credits: AUTHORS

   License: GNU General Public License Version 3 or later
            http://www.gnu.org/licenses/gpl.html
==============================================================================*/

#include <locale>
#include "centralManager.h"
#include "argParser.h"


using namespace std;

/*========================================================
    Input arguments:
        <funcname>
========================================================*/
void inter2_arg_error()
{
    printf("\nWrong input format. Correct format:\n");
    printf("\t<funcname>\n");
}
int main(int argc, char *argv[])
{
    setlocale(LC_CTYPE,"en_US.UTF-8");

    if (argc<2)
    {
        inter2_arg_error();
        return ARGNUM_ERROR;
    };


    CentralManager *cm;
    cm=new CentralManager();

    bool initResult=cm->Init(argv[1],ArgParser::FormFirstArgument());
    if(initResult!=SUCCESS)
        return initResult;
    cm->Start();
    qDebug() << "============================================";
    qDebug() << "Final result is " << cm->GetFinalRez()->GetAsStr();
    qDebug() << "============================================";
    printf("success\n");
    return SUCCESS;
}

