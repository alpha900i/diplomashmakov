#include "recursion.h"
#include "OptIGStructs.h"
#include "loops_optimization.h"

#include "../rig/rig.h"
#include "../rig/extref.h"


#include <algorithm>

bool nsp_recursion::optimize(OptIG &optig) {
  if (nullptr == nsp_o_loops::getRecCall(optig))
    return false; // функция не рекурсивная
  if (false == isTail(optig))
    return false; // рекурсия функции не хвостововая
  //for (auto nodeInter : *actors_)
  //  if (interpType == nodeInter->actor->GetActorType())
  //    1; // TODO: замена интерпретации циклом (ввести новый тип актора)
  return true;
}

bool nsp_recursion::isInterpret(OptIG &optig, ArcIGUnit* node) {
  auto actors_ = optig.actors();

  for (auto nodeInter : *actors_)
    if (interpType == nodeInter->actor->GetActorType()
        && node->num == nodeInter->num)
      return true;
  return false;
}

bool nsp_recursion::propagate(OptIG &optig, std::list<ArcIG*> &list) {
  if (list.empty()) return false;

  auto links_ = optig.links();
  ArcIG* startLink = list.front();
  list.pop_front();

  for (ArcIG* link : *links_)
    if (link->src == startLink->dst)
      list.push_back(link);
  return true;
}

template <class T>
void nsp_recursion::descent(OptIG &optig, ArcIGUnit *node
             , std::list<ArcIGUnit*> &list, T fun) {
  if (fun(optig, node)) list.push_back(node);

  std::list<ArcIG*> arcList;
  auto links_ = optig.links();

  for (ArcIG* link : *links_) // собираю дуги, выходящие из node в arcList
    if (link->src == *node)
      arcList.push_back(link);

  while (false == arcList.empty()) { // обхожу дерево, собираю вершины
    ArcIG* arc = arcList.front();
    if (fun(optig, &arc->dst))
      list.push_back(&arc->dst);
    nsp_recursion::propagate(optig, arcList);
  }

  // в list могли попасть одинаковые вершины
  list.unique();
}

bool nsp_recursion::isTail(OptIG &optig) {
  std::list<ArcIG*> recList; // список дуг, в конце(dst) которых вершины,
                            // обрабатывающиерезультаты рекурсивных вызовов
  std::list<ArcIGUnit*> interList; // список узлов, интерпретирующих
                                  // результаты рекурсивного вызова
  std::list<ArcIGUnit*> fList;  // список функций, посупающих на вход
                                // узлов interlist

  auto actors_ = optig.actors();
  auto links_ = optig.links();

  // формируем recList
  for (ActorData* nodeInter : *actors_)
    if (interpType == nodeInter->actor->GetActorType())
      for (auto link : *links_)
        if (link->dst.num == nodeInter->num
          && 1 == link->slot
          && link->src.type == ext
          && link->src.num == 0)
          recList.push_back(link);
  //
  for (int i = recList.size(); i >= 0; --i)
    propagate(optig, recList);
  //---

  // в список interList поместим вершины, интерпретирующие результаты
  // рекурсивных вызовов
  for (auto link : recList)
    descent(optig, &link->dst, interList, isInterpret);
  //---

  qDebug() << "-------------------Rec Interps:";
  for (ArcIGUnit* t : interList)
    qDebug() << t->num;
  qDebug() << "-------------------";

  // выбираю узлы, поступающие на функциональный вход интерпретаций
  // из списка interList в список fList
  for (ArcIGUnit* t : interList)
    for (ArcIG* link : *links_)
      if (1 == link->slot && link->dst == *t)
        fList.push_back(&link->src);
  //---

  // от каждого узла fList поднимаюсь вверх по графу и проверяю выйдет ли
  // в узле константа
  for (ArcIGUnit* t : fList)
    if (false == isConstFunc(optig, t))
      return false;
  //---
  return true;
}

bool nsp_recursion::isConstFunc(OptIG &optig, ArcIGUnit* unit) {
  auto actors_ = optig.actors();
  auto links_ = optig.links();

  ActorData *actor;
  ArcIGUnit tArgUnit;

  switch (unit->type) {
  case ext: // внешняя ссылка (вызов функции)
    return false;
  case spec: // специальная константа
    if (unit->num == questSpec || unit->num == dotSpec)
      return true;
    return false;
  case loc: // локальная константа
    return true;
  case act:
    for (ActorData* t : *actors_)
      if (t->num == unit->num)
        actor = t;

    switch (actor->actor->GetActorType()) {
    case argType: case retType:
      return true;
    case interpType:
      tArgUnit = dynamic_cast<InterpActor*>(actor->actor)->func;
      return isConstFunc(optig, &tArgUnit);
    case datalistType: case parlistType: case aslistType:
      for (ArcIG* link : *links_)
        if (link->dst == *unit && false == isConstFunc(optig, &link->src))
          return false;
      return true;
    }
  default:
    throw "nsp_recursion::isConstFunc: bad val";
  }
}
