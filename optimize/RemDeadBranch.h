#ifndef REMDEADBRANCH_H
# define REMDEADBRANCH_H
# include "OptIG.h"
void remDeadBranch(OptIG &);
  //!< удаляет неиспользуемый код
#endif // REMDEADBRANCH_H
