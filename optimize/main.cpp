#include "OptIG.h"
#include "RemDeadBranch.h"
#include "RemRepConsts.h"
#include "recursion.h"
#include "loops_optimization.h"

#include "../rig/rig.h"
#include "../rigparser/rigparser.h"

int main(int argc, char* argv[]) {
  QTextStream cout(stdout);

  if(argc < 1) {
      cout << "Incorrect number of arguments!" << endl;
      cout << "Use next format: optimize <input rig file name>"  << endl;
      return 1;
  }

  QString iFileName = argv[1];

  OptIG *optig = new OptIG(iFileName);

  OptIG *clone = nsp_o_loops::cloneFun(*optig, QString("opt_") + iFileName);
  QList<ActorData*> constArgs;

  int arity = nsp_o_loops::getConstParams(*optig, constArgs);
  int nComputes = nsp_o_loops::getConstComputations(*optig, constArgs);

  nsp_o_loops::loopInit(*optig, constArgs, nComputes, QString("opt_") + iFileName);
  nsp_o_loops::loopBody(*clone, constArgs, nComputes, arity);

  remDeadBranch(*optig);
  remDeadBranch(*clone);

  Rig *initOpt = optig->save();
  Rig *loopOpt = clone->save();

  initOpt->SaveText(QString("init_") + iFileName);
  loopOpt->SaveText(QString("opt_") + iFileName);

  return 0;
}
