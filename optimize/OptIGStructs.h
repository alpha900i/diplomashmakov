#ifndef OPTIGSTRUCTS_H
# define OPTIGSTRUCTS_H
# include "../rig/actor.h"
# include "../rig/value.h"
# include "../rig/extref.h"

//! внешняя ссылка в оптимизационном представнии
struct OptExtRef {
  int num;
  ExtRef *ref;
  OptExtRef(ExtRef* ref_, int num_)
    :num(num_), ref(ref_) {
  }
  OptExtRef(const OptExtRef& base)
    : num(base.num), ref(new ExtRef(*base.ref->GetName())) {
  }
};

//! узел дуги ОРИГ
struct ArcIGUnit {
  int     num;
                            //!< номер узла
  MemType type;
                            //!< тип узла
  ArcIGUnit() :num(-1) { }
  ArcIGUnit(int num_, MemType type_) : num(num_), type(type_) {
  }
  ArcIGUnit(MemLocation& mem) : num(mem.GetIndex()), type(mem.GetMemType()) {
    // implicit
  }
  ArcIGUnit(const ArcIGUnit& base)
    :num(base.num), type(base.type) {
  }

  bool operator==(const ArcIGUnit &a) const {
    return a.num == num && a.type == type;
  }
};

//! дуга ОРИГ
struct  ArcIG {
  int     slot;
                            //!< номер входа в приемнике
  ArcIGUnit src;
                            //!< источник данных
  ArcIGUnit dst;
                            //!< приемник данных

  ArcIG() {
  }
  ArcIG(const ArcIG& base)
    :slot(base.slot), src(base.src), dst(base.dst) {
  }
};

//! актор ОРИГ
struct ActorData {
  int     num;
                            //!< номер узла
  Actor   *actor;
                            //!< актор РИГ
  ActorData(Actor* actor_, int num_)
    :num(num_), actor(actor_) {
  }
  ActorData(const ActorData &base)
    :num(base.num) {
    switch (base.actor->GetActorType()) {
    case argType: actor = new BegActor(); break;
    case interpType:
      actor = new InterpActor(dynamic_cast<InterpActor*>(base.actor)->arg,
                          dynamic_cast<InterpActor*>(base.actor)->func);
      break;
    case datalistType: actor = new DataListActor(); break;
    case parlistType: actor = new ParListActor(); break;
    case aslistType: actor = new AsListActor(); break;
    case retType:
      actor = new RetActor(dynamic_cast<RetActor*>(base.actor)->arg);
      break;
    }
    actor->SetDelay(base.actor->GetDelay());

    if (dynamic_cast<ListActor*>(base.actor)) {
      for (MemLocation t : dynamic_cast<ListActor*>(base.actor)->memList) {
        MemLocation *tmp = new MemLocation(t);
        dynamic_cast<ListActor*>(actor)->memList << *tmp;
      }
    }
  }

  bool operator==(const ActorData &a) const {
                            //!< сравнение акторов по номеру
    return a.num == num;
  }

};

//! константа ОРИГ
struct ConstData {
  int     num;
                            //!< номер узла
  Value   *value;
                            //!< константа РИГ
  ConstData(Value* value_, int num_)
    :num(num_), value(value_) {
  }
  ConstData(const ConstData& base)
    :num(base.num) {
    switch(base.value->GetValueType()) {
    case IntValueType:
      value = new IntValue(dynamic_cast<IntValue*>(base.value)->GetIntValue());
      break;
    case BoolValueType:
      value = new BoolValue(dynamic_cast<BoolValue*>(base.value)->GetBoolValue());
      break;
    case FloatValueType:
      value = new FloatValue(dynamic_cast<FloatValue*>(base.value)->GetFloatValue());
      break;
    case CharValueType:
      value = new CharValue(dynamic_cast<CharValue*>(base.value)->GetCharValue());
      break;
    case StrValueType:
      value = new StrValue(dynamic_cast<StrValue*>(base.value)->GetStrValue());
      break;
    case SpecValueType:
      value = new SpecValue(dynamic_cast<SpecValue*>(base.value)->GetSpecValue());
      break;
    case DelayValueType:
      value = new DelayValue(dynamic_cast<DelayValue*>(base.value)->GetDelayNumber()
            , dynamic_cast<DelayValue*>(base.value)->GetDelayLink());
      break;
    //case ErrorValueType:
    //  value(new ErrorValue(dynamic_cast<ErrorValue*>(base.value)->get));
    //  break;
    case DataListValueType:
      value = new DataListValue();
      break;
    case ParListValueType:
      value = new ParListValue();
      break;
    case AsyncListValueType:
      // TODO: value(new Asyn(dynamic_cast<AsyncListValue*>(base.value)->value));
      break;
    }
  }
};

//! проверка эквивалентности двух констант РИГ
inline bool isEqual(Value& a, Value& b) {
  if (a.GetValueType() != b.GetValueType())
    return false;
  return a.GetAsStr() == b.GetAsStr();
}

#endif // OPTIGSTRUCTS_H
