# include "OptIGStructs.h"
# include "RemDeadBranch.h"

# include "../rig/rig.h"
# include "../rig/extref.h"

# include <algorithm>

void remDeadBranch(OptIG &optig) {
  QList<ArcIGUnit>  arcUnits; // список номеров используемых узлов

  QList<OptExtRef*>* functions_ = optig.functions();
  QList<ConstData*>* consts_ = optig.consts();
  QList<ActorData*>* actors_ = optig.actors();
  QList<ArcIG*>* links_ = optig.links();

  // помещаю в nodes возвратную вершину
  for (auto actor : *actors_)
    if (retType == actor->actor->GetActorType())
      arcUnits << ArcIGUnit(actor->num, act);

  // обход ОРИГ снизу вверх и сбор всех используемых вершин
  for (int i = 0; i < arcUnits.size(); ++i)
    for (ArcIG* link : *links_)
      if (link->dst == arcUnits[i]
          && 0 > arcUnits.indexOf(link->src))
        arcUnits << link->src;

  // сбор дуг, не вошедших в acrUnits
  QList<ArcIG*> remArcList;
  for (ArcIG* link : *links_)
    if (0 > arcUnits.indexOf(link->src)
        || 0 > arcUnits.indexOf(link->dst))
      remArcList << link;
  for (auto t : remArcList) {
    links_->removeOne(t);
    delete t;
  }

  // сбор констант, не вошедших в acrUnits
  QList<ConstData*> remConstList;
  for (ConstData *data : *consts_) {
    bool fl = false;
    for (ArcIGUnit t : arcUnits) {
      if (t.type == loc && data->num == t.num) {
        fl = true;
        break;
      }
    }
    if (false == fl)
      remConstList << data;
  }

  for (auto t : remConstList) {
    consts_->removeOne(t);
    delete t;
  }

  // сбор акторов, не вошедших в acrUnits
  QList<ActorData*> remActorList;
  for (ActorData *data : *actors_) {
    bool fl = false;
    for (ArcIGUnit t : arcUnits) {
       if (t.type == act && data->num == t.num) {
        fl = true;
        break;
      }
    }
    if (false == fl)
      remActorList << data;
  }

  for (auto t : remActorList) {
    actors_->removeOne(t);
    delete t;
  }

  // сбор внешних ссылок, не вошедших в acrUnits
  QList<OptExtRef*> remExtList;
  for (OptExtRef *data : *functions_) {
    bool fl = false;
    for (ArcIGUnit t : arcUnits) {
       if (t.type == ext && data->num == t.num) {
        fl = true;
        break;
      }
    }
    if (false == fl)
      remExtList << data;
  }

  for (auto t : remExtList) {
    functions_->removeOne(t);
    delete t;
  }
}
