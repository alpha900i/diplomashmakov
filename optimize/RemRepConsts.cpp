# include "OptIGStructs.h"
# include "RemRepConsts.h"

# include "../rig/rig.h"
# include "../rig/extref.h"

# include <algorithm>

void remRepConst(OptIG &optig) {
  auto consts_ = optig.consts();
  auto links_ = optig.links();
  qDebug() << "------------RepConst: ";
  for (auto it_a = consts_->begin(); consts_->end() != it_a; ++it_a) {
    for (auto it_b = it_a + 1; consts_->end() != it_b; ++it_b) {
      if (isEqual(*(*it_a)->value, *(*it_b)->value)) {
        // константа b повторяющаяся

        qDebug() << (*it_b)->num << " -> "
                 << (*it_b)->value->GetAsStr();

        ArcIGUnit tLink((*it_b)->num, MemType::loc);
        for (auto t : *links_)
          if (t->src == tLink) {
            qDebug() << t->src.num << "( " << t->src.type << " )" << " -> "
                     << t->dst.num << "( " << t->dst.type << " )";
            t->src.num = (*it_a)->num;
          }
        delete *it_b;
        consts_->removeOne(*it_b);
      }
    }
  }
}
