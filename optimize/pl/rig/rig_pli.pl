:- module(rig_pli, []).
:- use_module(strings), use_module(rig_db).

% Parallel List Interpretation
run:-
	getinerpar(Id, Delay, DataType, DataId, FunType, FunId), %write([Id, Delay]), nl,
	% получаю узлы интерпретации
	collect_inter_nodes(DataType, DataId, Datas),
	collect_inter_nodes(FunType, FunId, Funs),
	% убираю дуги, входящие в интерпретацию
	retract(rig_db:ig_edge(_SrcDate, (act, Id), 0)),
	retract(rig_db:ig_edge(_SrcFun, (act, Id), 1)),
	% операцию интерпретации заменяю операцией группировки в параллельный список
	retract(rig_db:ig_node(act, ":", Id, Delay)),
	assert(rig_db:ig_node(act, "[---]", Id, Delay)),
	%
	interpar(Datas, Funs, Id, Delay, 0), 
	%
	run, !;!.

%% интерпретация списка данных со списком функций
interpar([], _Funs, _ParId, _Delay, _Slot):-!.
interpar([HD|TD], Funs, ParId, Delay, Slot):-
	interdata(HD, Funs, ParId, Delay, Slot), !,
	length(Funs, NumFuns), NewSlot is Slot + NumFuns,
	interpar(TD, Funs, ParId, Delay, NewSlot).

%% интерпретация элемента данных со списком функций
interdata(_Data, [], _ParId, _Delay, _Slot):-!.
interdata(Data, [HF|TF], ParId, Delay, Slot):-
	Data = rig_db:ig_node(DataType, _DataVal, DataId, _DataDelay),
	HF = rig_db:ig_node(FunType, _FunVal, FunId, _FunDelay),
	%
	rig_db:add_node(act, ":", InterNode, Delay),
	assert(rig_db:ig_edge((DataType, DataId), (act, InterNode), 0)),
	assert(rig_db:ig_edge((FunType, FunId), (act, InterNode), 1)),
	%	
	assert(rig_db:ig_edge((act, InterNode), (act, ParId), Slot)),
	NewSlot is Slot + 1,
	interdata(Data, TF, ParId, Delay, NewSlot).
	
%% возвращает данные узла интерпретации с аргументами - параллельными списками,
%% а также узлы аргумента и функции
getinerpar(Id, Delay, DataType, DataId, FunType, FunId):-
	rig_db:ig_node(act, ":", Id, Delay),
	rig_db:ig_edge((DataType, DataId), (act, Id), 0),
	rig_db:ig_edge((FunType, FunId), (act, Id), 1), (
		DataType = act, rig_db:ig_node(act, "[---]", DataId, _DataDelay);
		FunType = act, rig_db:ig_node(act, "[---]", FunId, _FunDelay)
	), !.

%% собирает узлы для интерпретации
collect_inter_nodes(act, Id, Args):-
	rig_db:ig_node(act, "[---]", Id, _Delay), !,	% узел параллельного списка
	% собираю и сортирую по слоту список входящих дуг
	findall((SrcType, SrcNum, Slot), rig_db:ig_edge((SrcType, SrcNum), (act, Id), Slot), ArgEdges),
	predsort(rig_db:agr_slot_less, ArgEdges, SortedArgEdges),
	% формирую список узлов - аругментов списка
	maplist(inedge, SortedArgEdges, Args).
collect_inter_nodes(Type, Id, [rig_db:ig_node(Type, Val, Id, Delay)]):-
	rig_db:ig_node(Type, Val, Id, Delay), !.

%% возвращает узел начала дуги
inedge((Type, Id, _Slot), rig_db:ig_node(Type, Val, Id, Delay)):-
	rig_db:ig_node(Type, Val, Id, Delay).
	