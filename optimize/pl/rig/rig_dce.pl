:- module(rig_dce, []).
:- use_module(strings), use_module(rig_db).

% Dead Code Elimination
run:-
	rig_db:ig_node(Type, Val, Id, Delay), 
	Type \= bif, \+ (Type = act, Val = "return"), % вершина не BIF и не возвратная
	\+ rig_db:ig_edge((Type, Id), (_DstType, _DstId), _Slot),  % не имеет выходящих дуг
	findall(rig_db:ig_edge(X, (Type, Id), Y), rig_db:ig_edge(X, (Type, Id), Y), L), % L - список входящих дуг
	foreach((member(E, L), retract(E)), !),  % удаляю дуги
	retract(rig_db:ig_node(Type, Val, Id, Delay)), % удаляю не используюемую вершину
	run, !;!.
	