:- module(rig_db, []).
:- use_module(strings).
:- dynamic ig_node/4, ig_edge/3.

%% ig_node (node type, value, id, delay)
%% node type - (act|loc|ext|bif)
%% value - (457 | "qwerty" | : | arg | [---] | (---) | . | ...)

%% ig_edge(src, dst, input number)
%% src, dst - (node type, id)

%% для задержанных констант:
%%  node type - loc
%%  value - {---}
%%  id - номер константы
%%  delay - номер задержанного списка, представляемого константой
%% в задерженную константу входит дуга

%% bif - встроенные функции
init_bif:-
	assert(ig_node(bif, ".", 0, 0)),
	assert(ig_node(bif, "+", 1, 0)),
	assert(ig_node(bif, "-", 2, 0)),
	assert(ig_node(bif, "*", 3, 0)),
	assert(ig_node(bif, "/", 4, 0)),
	assert(ig_node(bif, "..", 5, 0)),
	assert(ig_node(bif, "dup", 6, 0)),
	assert(ig_node(bif, "<", 7, 0)),
	assert(ig_node(bif, "<=", 8, 0)),
	assert(ig_node(bif, "=", 9, 0)),
	assert(ig_node(bif, ">=", 10, 0)),
	assert(ig_node(bif, ">", 11, 0)),
	assert(ig_node(bif, "|", 12, 0)),
	assert(ig_node(bif, "?", 13, 0)),
	assert(ig_node(bif, "#", 14, 0)),
	assert(ig_node(bif, "[]", 15, 0)).

%% очистка БД
clear:-
	retract(ig_node(_, _, _, _)), clear;
	retract(ig_edge(_, _, _)), clear;!.

%% выводит РИГ в формате интерпретатора на текущее устройство вывода
dump:-
	rem_num_jump_all_types,

	findall((EVal, ENum, 0), ig_node(ext, EVal, ENum, _EDelay), ExtList),
	findall((LVal, LNum, LDelay), ig_node(loc, LVal, LNum, LDelay), LocList),
	findall((AVal, ANum, ADelay), ig_node(act, AVal, ANum, ADelay), ActList),
	
	predsort(node_id_less, ExtList, SortedExtList),
	predsort(node_id_less, LocList, SortedLocList),
	predsort(node_id_less, ActList, SortedActList),

	write('External'), nl, dump_RIG(ext, SortedExtList),
	write('Local'), nl, dump_RIG(loc, SortedLocList), 
	write('id	delay	operation	links		positions'), nl, dump_RIG(act, SortedActList), !.

%% сравнение узлов по id
node_id_less(Op, (_AVal, A, _ADelay), (_BVal, B, _BDelay)):-
	(A < B, !, Op = <; Op = >).

%% сравнение аргументов по слоту
agr_slot_less(Op, (_TypeA, _NumA, SlotA), (_TypeB, _NumB, SlotB)):-
	(SlotA < SlotB, !, Op = <; Op = >).

%% вывод аргументов актора (на вход подается сортированный по слоту список аргументов)
dump_RIG_edges([]):-!.
dump_RIG_edges([(bif, Num, _Slot)|T]):-
	!, ig_node(bif, Val, Num, _), string_to_list(SVal, Val),
	write(SVal), 
	(T \= [], !, write(' '); true),
	dump_RIG_edges(T).
dump_RIG_edges([(act, Num, _Slot)|T]):-
	!, write(Num), 
	(T \= [], !, write(' '); true),
	dump_RIG_edges(T).
dump_RIG_edges([(Type, Num, _Slot)|T]):-
	!, write(Type), write(':'), write(Num), 
	(T \= [], !, write(' '); true),
	dump_RIG_edges(T).

%% вывод внешних ссылок
dump_RIG(ext, []):-!.
dump_RIG(ext, [(Val, Num, _)|T]):-
	string_to_list(SVal, Val),
	write('	'), write(Num), write('	'), write(SVal), nl, 
	dump_RIG(ext, T).

%% вывод локальных констант
dump_RIG(loc, []):-!.
dump_RIG(loc, [('{---}', Num, Delay)|T]):-
	!, ig_edge((_ScrType, SrcNum), (loc, Num), _Slot),
	write('	'), write(Num), write('	'), 
	write('{'), write(Delay), write('}'), write(SrcNum), nl,
	dump_RIG(loc, T).
dump_RIG(loc, [(Val, Num, _Delay)|T]):-
	!, string_to_list(SVal, Val),
	write('	'), write(Num), write('	'), write(SVal), nl,
	dump_RIG(loc, T).

%% вывод акторов
dump_RIG(act, []):-!.
dump_RIG(act, [(Val, Num, Delay)|T]):-
	string_to_list(SVal, Val), 
	write(Num), write('	'), 
	write(Delay), write('	'), 
	write(SVal), write('		'), 

	findall((SrcType, SrcNum, Slot), ig_edge((SrcType, SrcNum), (act, Num), Slot), Args),
	predsort(agr_slot_less, Args, SortedArgs),

	dump_RIG_edges(SortedArgs),

	write('		pos 0 0 0 0'),
	nl,
	dump_RIG(act, T).
	
%% замена ссылок на узел ссылками на другой узел, слот не меняется
relinked(OldType, OldNum, NewType, NewNum):-
	retract(ig_edge((OldType, OldNum), (DstType, Dst), Slot)), !,
	assert(ig_edge((NewType, NewNum), (DstType, Dst), Slot)),
	relinked(OldType, OldNum, NewType, NewNum);
	retract(ig_edge((SrcType, SrcNum), (OldType, OldNum), Slot)), !,
	assert(ig_edge((SrcType, SrcNum), (NewType, NewNum), Slot)), 
	relinked(OldType, OldNum, NewType, NewNum);!.

%% замена номера узла со всеми ссылками
change_node_num(_, X, X):-!.
change_node_num(Type, OldNum, NewNum):-
	retract(ig_node(Type, Val, OldNum, Delay)), !,
	assert(ig_node(Type, Val, NewNum, Delay)),
	relinked(Type, OldNum, Type, NewNum);!.

%% принимает сортированный список узлов, начальный номер и тип узлов
%% устраняет разрывы в номерах
rem_num_jump_list([], _, _):-!.
rem_num_jump_list([H|T], Num, Type):- 
	change_node_num(Type, H, Num),
	!, NextNum is Num + 1, rem_num_jump_list(T, NextNum, Type).

%% удаление разрывов в номерах узлов заданного типа
rem_num_jump(Type):-
	findall(Id, ig_node(Type, _Val, Id, _Delay), Ids),
	sort(Ids, SortedIds), rem_num_jump_list(SortedIds, 0, Type).

%% удаление разрывов в номерах узлов всех типов
rem_num_jump_all_types:-
	rem_num_jump(act), 
  rem_num_jump(loc), 
  rem_num_jump(ext).

%% проверка вложенности задержанного списка А в список В
is_nested_dlists(A, A):-!. % считаем, что список вложен сам в себя 
is_nested_dlists(A, B):-
	ig_node(loc, '{---}', AId, A), !, % нахожу задержанную константу А
	ig_edge((loc, AId), (act, CId), _Slot), % нахожу актор C, зависящий по данным от А
	ig_node(act, _CType, CId, CDelay), % нахожу задержанный список узла C
	is_nested_dlists(CDelay, B). % A ~> B если существует C: A ~> C, C ~> B

%% возвращает не занятый идентификатор узла заданного типа
free_id(Type, TI, I):-
	\+ ig_node(Type, _, TI, _), !, I is TI;
	TTI is TI + 1, free_id(Type, TTI, I).

%% добавляет в базу новый узел (ID присваивается автоматически)
add_node(Type, Val, Id, Delay):-
	free_id(Type, 0, Id),
	assert(ig_node(Type, Val, Id, Delay)).
	