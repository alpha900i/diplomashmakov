:- module(rig_rce, []).
:- use_module(strings), use_module(rig_db).

% Repeated Constants Elimination
run:-
	rig_db:ig_node(loc, Val, AId, _ADelay), Val \= '{---}', % ищу не задержанную константу
	rig_db:ig_node(loc, Val, BId, BDelay), AId \= BId, % ищу еще одну константу со значением Val, но другим Id
	
	rig_db:relinked(loc, BId, loc, AId),  % заменяю ссылки на B ссылками на A
	retract(rig_db:ig_node(loc, Val, BId, BDelay)), % удаляю B
	run, !;!.
	