:- module(rig_rnndle, []).
:- use_module(strings), use_module(rig_db).

% Repeated Nodes in Nested Delay List Elimination
run:-
	rig_db:ig_node(act, Val, AId, ADelay),
	rig_db:ig_node(act, Val, BId, BDelay), 
	AId \= BId, 
	rig_db:is_nested_dlists(BDelay, ADelay), 	% узел В находится в задержанном списке, 
																						% вложенном в задержанный список узла А
	% если дуга входит в - то она входит в В, и наоборот:
	forall(rig_db:ig_edge(X, (act, AId), Slot), rig_db:ig_edge(X, (act, BId), Slot)), 
	forall(rig_db:ig_edge(X, (act, BId), Slot), rig_db:ig_edge(X, (act, AId), Slot)),

	% дуги, входящие в узел В больше не нужны (он будет замене узлом В)
	foreach(retract(rig_db:ig_edge(X, (act, BId), Slot)), !),
	rig_db:relinked(act, BId, act, AId),  % заменяю ссылки на B ссылками на A

	retract(rig_db:ig_node(act, Val, BId, BDelay)), % удаляю B
	run, !;!.

