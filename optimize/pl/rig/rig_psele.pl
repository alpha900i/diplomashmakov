:- module(rig_psele, []).
:- use_module(strings), use_module(rig_db).

% Parallel Single-Element List Elimination
run:-
	rig_db:ig_node(act, "[---]", Id, Delay), 
	findall(X, rig_db:ig_edge(X, (act, Id), _), [(IType, IId)]), % входит лишь одна дуга

	rig_db:relinked(act, Id, IType, IId), % заменяю ссылку на список, ссылкой на его единственный элемент
	
	retract(rig_db:ig_node(act, "[---]", Id, Delay)), % удаляю вершину одноэлементного списка
	run, !;!.
	