:- module(rig_nple, []).
:- use_module(strings), use_module(rig_db).

% Nested Parallel Lists Elimination
run:-
	rig_db:ig_node(act, AType, AId, _ADelay), (AType = "[---]"; AType = "(---)"),
	rig_db:ig_edge((act, BId), (act, AId), ABSlot),
	rig_db:ig_node(act, "[---]", BId, _BDelay),
	!, % B вложен в А
	findall((EBType, EBId, EBSlot), rig_db:ig_edge((EBType, EBId), (act, BId), EBSlot), EBL),
	findall((EAType, EAId, EASlot), rig_db:ig_edge((EAType, EAId), (act, AId), EASlot), EAL),
	length(EBL, LEBL),

	retract(rig_db:ig_edge((act, BId), (act, AId), ABSlot)),
	!, % дуги EBL входят в B, EAL - в A
	% увеличение номера слота у дуг, входящих в А на длину списка В - 1 
	% (потому что дуга от паралельного списка удалена)
	foreach((
		member((E_AType, E_AId, E_ASlot), EAL), 
		E_ASlot > ABSlot, 
		retract(rig_db:ig_edge((E_AType, E_AId), (act, AId), E_ASlot)),
		EE_ASlot is E_ASlot + LEBL - 1,
		assert(rig_db:ig_edge((E_AType, E_AId), (act, AId), EE_ASlot))
	), !),

	% переброс дуг из В в А
	foreach((
		member((E_BType, E_BId, E_BSlot), EBL),
		EE_BSlot is E_BSlot + ABSlot,
		rig_db:ig_edge((E_BType, E_BId), (act, BId), E_BSlot),
		assert(rig_db:ig_edge((E_BType, E_BId), (act, AId), EE_BSlot))
	), !), 
	run; 
	!. % нет вложенных списков