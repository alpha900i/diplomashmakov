:- module(rig_parser, []).
:- use_module(strings), use_module(rig_db).

run:-
	rig_parser('ExternalHead'), !.

rig_parser('ExternalHead'):-
	rd_line(_Str), !, rig_parser('External').

rig_parser('External'):- 
	rd_line(Str), !, (
		Str <- ["Local", _], !, rig_parser('Local');
		Str <- ["	", CId, "	", Fname], !, number_codes(Id, CId),
			assert(rig_db:ig_node(ext, Fname, Id, 0)), 
			rig_parser('External')
	).

rig_parser('Local'):- 
	rd_line(Str), !, (
		Str <- ["id	delay	operation	links		positions", _], !, rig_parser('Actor');
		Str <- ["	", CId, "	{", CDelayNum, "}", CNodeNum], !,
			number_codes(Id, CId), number_codes(DelayNum, CDelayNum), number_codes(NodeNum, CNodeNum),
			assert(rig_db:ig_node(loc, '{---}', Id, DelayNum)), 
			assert(rig_db:ig_edge((act, NodeNum), (loc, Id), 0)),
			rig_parser('Local');
		Str <- ["	", CId, "	", Val], !,
			number_codes(Id, CId),
			assert(rig_db:ig_node(loc, Val, Id, 0)), 
			rig_parser('Local')
	).

rig_parser('Actor'):-
	rd_line(Str), !, (
		Str <- [CId, "	", CDelayNum, "	", Op, "		", Inps, "		pos", _], !, (
			number_codes(Id, CId), number_codes(DelayNum, CDelayNum),
			assert(rig_db:ig_node(act, Op, Id, DelayNum)),
			parseInps(Id, 0, Inps)
		), rig_parser('Actor');!
	).

parseInps(_, _, []):-!.
parseInps(Id, InpNum, Inps):-
	Inps <- [Inp, " ", TailInps], !, 
		parseInp(Id, InpNum, Inp), 
		NextInpNum is InpNum + 1, 
		parseInps(Id, NextInpNum, TailInps);

	parseInp(Id, InpNum, Inps).

parseInp(Id, InpNum, Val):-
	% дуга из актора:
	strings:is_num(Val), !, 
	number_codes(InpId, Val), assert(rig_db:ig_edge((act, InpId), (act, Id), InpNum)); 
	% дуга из локальной константы
	Val <- ["loc:", LocVal], !, 
	number_codes(InpId, LocVal), assert(rig_db:ig_edge((loc, InpId), (act, Id), InpNum)); 
	% дуга из внешней ссылки
	Val <- ["ext:", ExtVal], !, 
	number_codes(InpId, ExtVal), assert(rig_db:ig_edge((ext, InpId), (act, Id), InpNum));
	% дуга из встроенной функции
	rig_db:ig_node(bif, Val, ValId, _), !, 
	assert(rig_db:ig_edge((bif, ValId), (act, Id), InpNum)).