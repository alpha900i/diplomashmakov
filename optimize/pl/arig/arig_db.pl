:- module(arig_db, []).
:- use_module(strings).
:- dynamic aig_node/5, aig_edge/3.

%% aig_node (node type, value, id, delay, arity)

%% проверка отсутствия оперторов []
check:-
	aig_node(act, "[]", _Id, _Delay, _Arity), 
	write('ARIG should not contain a list of the conversion operation in a parallel list'),
	!, fail;!.

%% создание АРИГ по РИГ
make_arig:-
	% создание узлов
	retract(rig_db:ig_node(Type, Val, Id, Delay)), !,
	assert(aig_node(Type, Val, Id, Delay, not_init)), make_arig;
	% создание дуг
	retract(rig_db:ig_edge(Src, Dst, Num)), !,
	assert(aig_edge(Src, Dst, Num)),  make_arig;
	% проверка АРИГ и установка арности
	check, set_arity.

%% правила вычисления арности узлов
arity(ext, _Val, _Id, _Delay, 1):-!.
arity(bif, _Val, _Id, _Delay, 1):-!.
arity(loc, _Val, _Id, _Delay, 1):-!.
arity(act, "(---)", _Id, _Delay, 1):-!.
arity(act, "arg", _Id, _Delay, 1):-!.
arity(act, "return", _Id, _Delay, 1):-!.

arity(act, "[---]", Id, _Delay, Arity):-
	findall((SrcType, SrcId), aig_edge((SrcType, SrcId), (act, Id), _Slot), SrcList), !,
	sum_arity(SrcList, Arity).

arity(act, ":", Id, _Delay, 1):-
	aig_node(bif, "?", EId, _EDelay, _Arity), 
	aig_edge((bif, EId), (act, Id), "1"), !.

arity(act, ":", Id, _Delay, Arity):-
	findall((SrcType, SrcId), aig_edge((SrcType, SrcId), (act, Id), _Slot), SrcList), 
	mul_arity(SrcList, Arity), !.

% вычисляет сумму арностей дуг списка
sum_arity([], 0):-!.
sum_arity([(EType, EId)|T], R):-
	sum_arity(T, TR),
	aig_node(EType, _Val, EId, _Delay, Arity),
	Arity \= not_init,
	R is TR + Arity.

% вычисляет произведение арностей дег списка
mul_arity([], 1):-!.
mul_arity([(EType, EId)|T], R):-
	mul_arity(T, TR),
	aig_node(EType, _Val, EId, _Delay, Arity),
	Arity \= not_init,
	R is TR * Arity.

%% установка арности
set_arity:-
	aig_node(Type, Val, Id, Delay, not_init),
	arity(Type, Val, Id, Delay, Arity), 
	retract(aig_node(Type, Val, Id, Delay, not_init)),
	assert(aig_node(Type, Val, Id, Delay, Arity)), set_arity;
	% проверка того, что все узлы получили арность
	forall(aig_node(_Type, _Val, _Id, _Delay, Arity), Arity \= not_init).

clear:-
	retract(aig_node(_, _, _, _, _)), clear;
	retract(aig_edge(_, _, _)), clear;!.

%% вывод узлов в читаемом формате
dump_nodes:-
	aig_node(Type, Val, Id, Delay, Arity),
	(
		Val == '{---}', SId ? Id, SDelay ? Delay, 
		write(aig_node(Type, '{---}', SId, SDelay, Arity));

		Val \== '{---}', SVal ? Val, SId ? Id, SDelay ? Delay,
		write(aig_node(Type, SVal, SId, SDelay, Arity))
	), nl, fail; !.

%% вывод дуг
dump_edges:-
	aig_edge((SrcType, Src), (DstType, Dst), Slot),
	SSrc ? Src, SDst ? Dst, SSlot ? Slot,
	write(aig_edge((SrcType, SSrc), (DstType, SDst), SSlot)), nl, fail; !.