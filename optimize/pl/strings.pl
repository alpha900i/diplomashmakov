:- module(strings, [
	op(200, xfy, <-), (<-)/2, 
	rd_line/1, 
	op(200, xfy, ?), (?)/2]).

%% проверяет возможность разложения строки R на подстроки из списка
%% "asdf" <- [X, "d", Y] => X = "as", Y = "f"
R <- ([A, B]):-
	!, append(A, B, R).
R <- ([A|T]):-
	append(A, TR, R),
	TR <- T,
	append(A, TR, R), !.

%% выполняет преобразование строки в список кодов и наоборот
[] ? ([]):-!.
[HS|TS] ? ([H|T]):-
	char_code(HS, H), TS ? T.

%% считывает список кодов с текущего устройства ввода
rd_line(Str):-
	get_code(H), (
		code_type(H, end_of_line), !, Str = [];
		code_type(H, end_of_file), !, Str = [];
		Str = [H|T], rd_line(T)
	).

%% проверяет является ли список кодов целым беззнаковым числом
is_num([]):-!.
is_num([H|T]):- is_digit(H, 10, _), is_num(T).




