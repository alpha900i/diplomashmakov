:- module(trans, [goal/0]).
:- notrace.
:- use_module(strings), 
   use_module(rig/rig_db), use_module(rig/rig_parser),
	 use_module(rig/rig_nple),
	 use_module(rig/rig_dce),
	 use_module(rig/rig_rce),
	 use_module(rig/rig_psele),
	 use_module(rig/rig_rnndle),
	 use_module(rig/rig_pli).

goal:-
	% подключение модуля разбора строк
	[strings], 
	% подключение модуля с БД РИГ и модуля парсера РИГ
	[rig/rig_db], [rig/rig_parser],
	% очистка БД РИГ, инициализация встроенных функций (БИФ)
	rig_db:clear, rig_db:init_bif,
	%
	open('1.rig', read, RIGStream), !, set_input(RIGStream),
	% заполнение БД РИГ
	rig_parser:run, !, 
	
	close(RIGStream), 

	open('_1.rig', write, ResultStream), !, set_output(ResultStream),
/*
	% удаление вложенных параллельных списков
	[rig/rig_nple],
	rig_nple:run,

	% удаление мертвого кода
	[rig/rig_dce],
	rig_dce:run,

	% удаление повторяющихся констант
	[rig/rig_rce],
	rig_rce:run,

	% удаление одноэлементных списков
	[rig/rig_psele],
	rig_psele:run,

	% удаление повторяющихся узлов во вложенных задержанных списках
	[rig/rig_rnndle],
	rig_rnndle:run,
*/
	% интерпретация параллельных списков
	[rig/rig_pli], 
	rig_pli:run,

	rig_db:dump,
	
	close(ResultStream),
	true;!.

p(A, B):-
	rig_db:is_nested_dlists(A, B), write(yes), !; write(no), !.