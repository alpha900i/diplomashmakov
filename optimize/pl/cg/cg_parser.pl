:- module(cg_parser, []).
:- use_module(strings), use_module(cg_db).

run:-
	cg_parser('fname'), !.

%% парсер УГ
cg_parser('fname'):- 
	rd_line(_Str), cg_parser('header').
cg_parser('header'):- 
	rd_line(_Header), cg_parser('cg_line').
cg_parser('cg_line'):- 
	rd_line(Str), (
		Str <- ["Signals", _], !, cg_parser('signals');
		proc_cg_line(Str), cg_parser('cg_line')
	).
cg_parser('signals'):-
	rd_line(Str), (
		Str <- ["Dynamic links:", _], !, cg_parser('dynamic_links');
		Str <- [Number, "	", Node, "	", Input], !,
		assert(cg_db:cg_sig(Number, Node, Input)), 
		cg_parser('signals')
	).
cg_parser('dynamic_links'):-
	rd_line(Str), (
		Str <- [Number, "	", Delay_List, "	", Node], !,
		assert(cg_db:cg_dyn_links(Number, Delay_List, Node)), 
		cg_parser('dynamic_links'); !
	).
		
%% обработка строки с описанием узла
proc_cg_line(L0):-
	L0 <- [Id, "	", Delay, "	", Automat, ",", AutomatVer, "		", Inode, "	", L1], 
	assert(cg_db:cg_node(Id, Delay, Automat, AutomatVer, Inode)),
	( L1 <- ["links:", Links], !, proc_cg_links(Id, Links); ! ). % вершина return не содержит "links:"

%% обработка дуг
proc_cg_links(_, []):-!.
proc_cg_links(Id, L0):-
	L0 <- [Dst, ",", Slot, ";", L1],
	assert(cg_db:cg_edge(Id, Dst, Slot)),
	proc_cg_links(Id, L1).
	
	

