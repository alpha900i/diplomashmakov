:- module(cg_rem_rep_edges, []).
:- use_module(strings), use_module(cg_db), use_module(cg_parser).

run:-
	p([]),!.

%% собирает все зависимости для вершины
all_depend([], VN, VN):-!.
all_depend([X|T], VN, Buff):-
	findall(Y, step(Y, VN, X), AdjNodes),
	append(T, AdjNodes, NT),
	append(VN, AdjNodes, NVN),
	all_depend(NT, NVN, Buff). 

%% если при удалении дуги зависимости не изменились - дуга лишняя
p(Proc):-
	cg_db:cg_edge(X, Y, Slot), 
	not(member((X, Y, Slot), Proc)),
	all_depend([Y], [], Dep_bef), 
	retract(cg_db:cg_edge(X, Y, Slot)), 
	all_depend([Y], [], Dep_aft),
	(
		length(Dep_aft, Len), length(Dep_bef, Len), !, write(cg_edge(X, Y, Slot)), nl;
		assert(cg_db:cg_edge(X, Y, Slot))
	),
	p([(X, Y, Slot)|Proc]);!.
	

%% возвращает Y (отсутствующий в VN), из которого можно попасть в X. 
step(X, VN, Y):-
  cg_db:cg_edge(X, Y, _),
  not(member(X, VN)).
