:- module(cg_db, []).
:- use_module(strings).
:- dynamic cg_node/5, cg_edge/3, cg_sig/3, cg_dyn_links/3.

%% очиска всех БД
clear:-
	retract(cg_node(_, _, _, _, _)), clear;
	retract(cg_edge(_, _, _)), clear;
	retract(cg_sig(_, _, _)), clear;
	retract(cg_dyn_links(_, _, _)), clear;!.

%% вывод узлов в читаемом формате
dump_nodes:-
	cg_node(Id, Delay, Automat, AutomatVer, Inode),
	SId ? Id, SDel ? Delay, SAut ? Automat, SAutV ? AutomatVer, SInode ? Inode,
	write(cg_node(SId, SDel, SAut, SAutV, SInode)), nl, fail; !.

%% вывод дуг
dump_edges:-
	cg_edge(Src, Dst, Slot),
	SSrc ? Src, SDst ? Dst, SSlot ? Slot,
	write(cg_edge(SSrc, SDst, SSlot)), nl, fail; !.

%% вывод сигналов
dump_sig:-
	cg_sig(Number, Node, Input),
	SNum ? Number, SNode ? Node, SIn ? Input,
	write(cg_sig(SNum, SNode, SIn)), nl, fail; !.

%% вывод динамических сигналов
dump_dyn_sig:-
	cg_dyn_links(Number, Delay_List, Node),
	SNum ? Number, SDel ? Delay_List, SNode ? Node,
	write(cg_dyn_links(SNum, SDel, SNode)), nl, fail; !.