#ifndef RECURSION_H
# define RECURSION_H
# include "OptIG.h"

namespace nsp_recursion {
  bool optimize(OptIG &optig);
    //!< замена хвостовой рекурсии циклом

  bool isTail(OptIG &optig);
    //! проверяет рекурсия функции хвостовой

  bool isConstFunc(OptIG &optig, ArcIGUnit* node);
    //! проверяет будет ли узел node константой или списком констант

  bool propagate(OptIG &optig, std::list<ArcIG*> &list);
    //! выбирает первую дугу из списка, добавляет в него дуги, исходящие из
    //! ее конца
    //! @return false если а входе пустой список, иначе - true

  template <class T>
  void descent(OptIG &optig, ArcIGUnit* node
               , std::list<ArcIGUnit*> &list, T fun);
    //! \brief выполняет спуск по ОРИГ с заданной вершины node
    //! собирает все вершины, для которых выполнислоь условие fun
    //! в список list

  bool isInterpret(OptIG &optig, ArcIGUnit* node);
    //! проверяет является ли узел интерпретацией

}
#endif // RECURSION_H
