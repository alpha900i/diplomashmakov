//! класс оптимизационного внутреннего представления ИГ ФПЯП "Пифагор"
#ifndef OPTIG_H
# define OPTIG_H
# include <QObject>
class Rig;
struct ArcIG;
struct ArcIGUnit;
struct ActorData;
struct ConstData;
class OptExtRef;

//! класс оптимизационного РИГ
class OptIG: public QObject {
Q_OBJECT
public:
  OptIG(QObject *parent = 0);
  OptIG(QString rigFname, QObject *parent = 0);
                              //!< загружает РИГ с файла, преобразует его OptIG
  virtual ~OptIG();

  bool load(Rig*);
                              //!< преобразование РИГ -> ОРИГ
  Rig* save();
                              //!< преобразование ОРИГ -> РИГ
  QList<ArcIG*>* links();
                              //!< возвращает указатель на links_
  QList<ActorData*>* actors();
                              //!< возвращает указатель на actors_
  QList<ConstData*>* consts();
                              //!< возвращает указатель на consts_
  QList<OptExtRef*>* functions();
                              //!< возвращает указатель на functions_

  void links(QList<ArcIG*>* links);
                              //!< возвращает указатель на links_
  void actors(QList<ActorData*>* actors);
                              //!< возвращает указатель на actors_
  void consts(QList<ConstData*>* consts);
                              //!< возвращает указатель на consts_
  void functions(QList<OptExtRef*>* functions);
                              //!< возвращает указатель на functions_

#ifdef __DEBUG__
  void testOut();
  //!< вывод ОРИГ в qDebug
  //!< доступно только при наличии макроса __DEBUG__
#endif

protected:

  QList<ArcIG*>       links_;
                              //!< дуги
  QList<ActorData*>   actors_;
                              //!< акторы
  QList<OptExtRef*>   functions_;
                              //!< внешние ссылки
  QList<ConstData*>   consts_;
                              //!< локальные константы
  int getDstSlot(ArcIGUnit&);
                              //!< возвращает слот приемника для узла дуги
  void normalize();
                              //!< нормализация РИГ перед выводом
};

#endif // OPTIG_H
