#include "OptIG.h"
#include "OptIGStructs.h"

#include "../rig/rig.h"
#include "../rig/extref.h"
#include "../rigparser/rigparser.h"

#include <algorithm>
#include <iostream>
// -----------------------------------------------------------------------------
// ------------------------------------ GETTERs / SETTERs
// -----------------------------------------------------------------------------
QList<ArcIG*>* OptIG::links() { return &links_; }
QList<ActorData*>* OptIG::actors() { return &actors_; }
QList<ConstData*>* OptIG::consts() { return &consts_; }
QList<OptExtRef *> *OptIG::functions() { return &functions_; }

void OptIG::links(QList<ArcIG*>* links) { links_ = *links; }
void OptIG::actors(QList<ActorData*>* actors) { actors_ = *actors; }
void OptIG::consts(QList<ConstData*>* consts) { consts_ = *consts; }
void OptIG::functions(QList<OptExtRef*>* functions) { functions_ = *functions; }
// -----------------------------------------------------------------------------
// ------------------------------------ CST/DST
// -----------------------------------------------------------------------------
OptIG::OptIG(QObject *parent): QObject(parent) { }

OptIG::OptIG(QString rigFname, QObject *parent): QObject(parent) {
  QTextStream cout(stdout);
  Rig rig;

  RigParser parser(&rig, rigFname);
  parser.Start();

  rig.TestOut(cout);
  load(&rig);
  //this->testOut();
}

OptIG::~OptIG() {
  for (auto t : links_) delete t;
  for (auto t : actors_) delete t;
  for (auto t : consts_) delete t;
  links_.clear();
  functions_.clear();
  consts_.clear();
}
// -----------------------------------------------------------------------------
// ------------------------------------ normalize
// -----------------------------------------------------------------------------
void OptIG::normalize() {
  /* нормализация констант */
  // --сортирую константы по индексу
  std::sort(consts_.begin(), consts_.end()
      , [](const ConstData* a, const ConstData* b) {
    return a->num < b->num;
  });
  // --устраняю разрывы между номерами консант
  int idx = 0;
  for (auto t : consts_) {
    if (t->num != idx) {
      ArcIGUnit tLink(t->num, MemType::loc);
      for (auto link : links_)
        if (link->src == tLink)
          link->src.num = idx;
      t->num = idx;
    }
    ++idx;
  }
  /* нормализация акторов */
  // сортирую по индексу:
  std::sort(actors_.begin(), actors_.end()
      , [](const ActorData* a, const ActorData* b) {
    return a->num < b->num;
  });
  // устраняю разрывы в номерах акторов
  idx = 0;
  for (auto t : actors_) {
    if (t->num != idx) {
      ArcIGUnit tLink(t->num, MemType::act);
      for (auto link : links_) {
        if (link->src == tLink)
          link->src.num = idx;
        if (link->dst == tLink)
          link->dst.num = idx;
      }
      t->num = idx;
    }
    ++idx;
  }


  // ссылки в акторах должны соответствовать дугам ОРИГ
  for (auto link : links_) {
    if (link->dst.type == loc)
      continue; // проброщенная дуга через задержанный список
    Actor *dst = actors_[link->dst.num]->actor; // актор-приемник данных
    switch(dst->GetActorType()) {
    case argType:
      break;
    case interpType:
      if (0 == link->slot)// дуга с аргументом
        dynamic_cast<InterpActor*>(dst)->arg.Set(link->src.type, link->src.num);
      else if (1 == link->slot)// дуга с оператором
        dynamic_cast<InterpActor*>(dst)->func.Set(link->src.type, link->src.num);
      break;
    case datalistType:
    case parlistType:
    case aslistType:
      while (dynamic_cast<ListActor*>(dst)->memList.size() <= link->slot) {
        MemLocation *tmp = new MemLocation();
        dynamic_cast<ListActor*>(dst)->memList << *tmp;
      }
      dynamic_cast<ListActor*>(dst)->memList[link->slot]
          .Set(link->src.type, link->src.num);
      break;
    case retType:
      dynamic_cast<RetActor*>(dst)->arg.Set(link->src.type, link->src.num);
      break;
    }
  }
}
// -----------------------------------------------------------------------------
// ------------------------------------ save/load
// -----------------------------------------------------------------------------
Rig* OptIG::save() {
  normalize();
  Rig *rig = new Rig;
  for (auto t : consts_)
    rig->Append(t->value);
  for (auto t : actors_)
    rig->Append(t->actor);
  for (auto t : functions_)
    rig->Append(t->ref);

  return rig;
}

bool OptIG::load(Rig* rig) {
  int idx = -1;
  // загрузка функций
  auto extRefs = rig->GetExtRefList();
  idx = 0;
  for (auto t : *extRefs)
    functions_ << new OptExtRef(t, idx++);
  // загрузка констант
  auto rigConsts = rig->GetlocalConstList();
  idx = 0;
  for (auto t : *rigConsts)
    consts_ << new ConstData(t, idx++);
  // загрузка операторов
  auto rigActors = rig->GetActorList();
  idx = 0;
  for (auto t : *rigActors)
    actors_ << new ActorData(t, idx++);
  // построение списка дуг
  for (auto t : actors_) {
    ArcIG *link;
    switch(t->actor->GetActorType()) {
    case argType:
      break;
    case interpType:
      link = new ArcIG;
      link->dst.num = t->num;
      link->dst.type = act;
      link->src = dynamic_cast<InterpActor*>(t->actor)->arg;
      link->slot = 0;
      links_ << link;
      link = new ArcIG;
      link->dst.num = t->num;
      link->dst.type = act;
      link->src = dynamic_cast<InterpActor*>(t->actor)->func;
      link->slot = 1;
      links_ << link;
      break;
    case datalistType:
    case parlistType:
    case aslistType:
      idx = 0;
      for (auto mem : dynamic_cast<ListActor*>(t->actor)->memList) {
        link = new ArcIG;
        link->dst.num = t->num;
        link->dst.type = act;
        link->src = mem;
        link->slot = idx++;
        links_ << link;
      }
      break;
    case retType:
      link = new ArcIG;
      link->dst.num = t->num;
      link->dst.type = act;
      link->src = dynamic_cast<RetActor*>(t->actor)->arg;
      link->slot = 0;
      links_ << link;
      break;
    }
  }

  // пробрасываем дугу через задержанную константу
  for (ConstData* t : consts_) {
    if (ValueType::DelayValueType == t->value->GetValueType()) {
      // задержанная константа
      ArcIG* link = new ArcIG;
      link->dst.num = t->num;
      link->dst.type = loc;
      link->src.num = dynamic_cast<DelayValue*>(t->value)->GetDelayLink();
      link->src.type = act;
      link->slot = 0;
      links_ << link;
    }
  }

  return true;
}
// -----------------------------------------------------------------------------
// ------------------------------------ helper funs
// -----------------------------------------------------------------------------
int OptIG::getDstSlot(ArcIGUnit& unit) {
    int num = -1;
    for (const auto link : links_)
        if (link->dst == unit && num < link->dst.num)
            num = link->dst.num;
    if (num < 0) num = 0;
    else ++num;
    return num;
}

#ifdef __DEBUG__
void OptIG::testOut() {
  qDebug() << "------------OPTIG::CONSTs:";
  for (auto t : consts_)
    qDebug() << t->num << " -> "
             << t->value->GetAsStr();
  qDebug() << "------------OPTIG::ACTORs:";
  for (auto t : actors_)
    qDebug() << t->num << " -> "
             << t->actor->GetAsStr();
  qDebug() << "------------OPTIG::LINKs:";
  for (auto t : links_)
    qDebug() << t->src.num << "( " << t->src.type << " )" << " -> "
             << t->dst.num << "( " << t->dst.type << " )" << "**" << t->slot;
}
#endif
