#include "loops_optimization.h"
#include "OptIGStructs.h"

# include "../rig/value.h"

#include <QList>

void nsp_o_loops::loopBody(OptIG &optig, QList<ActorData *> &constComputes
                           , int nComputes, int oldArity) {

  auto consts_ = optig.consts();
  auto links_ = optig.links();
  auto actors_ = optig.actors();

  // дополнение ОРИГ кодом получения значений константных выражений
  int i = constComputes.size() - nComputes; // устанавливаю индекс
                                            // начала константных выражений
  int arity = oldArity;
  for (; i < constComputes.size(); ++i) {
    ++arity;
    //----- дополнение ОРИГ константами для получения аргументов
    ConstData *tCs = new ConstData(new IntValue(arity), consts_->size());
    consts_->append(tCs);

    ActorData* tOp = new ActorData(new InterpActor(
            MemLocation(act, 0), MemLocation(loc, tCs->num)), actors_->size());
    tOp->actor->SetDelay(0);
    actors_->append(tOp);

    ArcIG *tArc = new ArcIG();
    tArc->dst.num = tOp->num;
    tArc->dst.type = act;
    tArc->src.num = tCs->num;
    tArc->src.type = loc;
    tArc->slot = 1; // функция
    links_->append(tArc);

    tArc = new ArcIG();
    tArc->dst.num = tOp->num;
    tArc->dst.type = act;
    tArc->src.num = 0;
    tArc->src.type = act;
    tArc->slot = 0; // аргумент
    links_->append(tArc);

  // замена старых ссылок (на элементы constComputes на новые)
    foreach (ArcIG* link, *links_)
      if (link->src.num == constComputes[i]->num && link->src.type == act)
        link->src.num = tOp->num;
  }

}

void nsp_o_loops::loopInit(OptIG &optig, QList<ActorData*> &constComputes
              , int nComputes, QString fLoopName) {
  auto actors_ = optig.actors();
  auto links_ = optig.links();
  QList<OptExtRef*>* functions_ = optig.functions();

  // перенесем константные выражения в нулевой задержанный список
  for (ActorData* t : constComputes)
    for (ActorData* tt : *actors_)
      if (tt->num == t->num) {
        tt->actor->SetDelay(0);
        break;
      }

  // добавляю внешнюю ссылку
  int extIdx = functions_->size(); // номер новой внешней ссылки
  functions_->append(new OptExtRef(new ExtRef(fLoopName), extIdx));

  // выполним над аргументом операции [] и ()
  // (добавим эти узлы в ОРИГ)
  ActorData* tOp = new ActorData(new InterpActor(
          MemLocation(ext, -1), MemLocation(ext, -1)), actors_->size());
  tOp->actor->SetDelay(0);
  actors_->append(tOp);

  ArcIG *tArc = new ArcIG();
  tArc->dst.num = tOp->num;
  tArc->dst.type = act;
  tArc->src.num = palistSpec;
  tArc->src.type = spec;
  tArc->slot = 1; // функция
  links_->append(tArc);

  tArc = new ArcIG();
  tArc->dst.num = tOp->num;
  tArc->dst.type = act;
  tArc->src.num = 0;
  tArc->src.type = act;
  tArc->slot = 0; // аргумент
  links_->append(tArc);

  int argIdx = actors_->size();// индекс аргумента функции цикла (списка данных)
  ActorData* arg;      // аргумент функции цикла
                      // аругмент сейчас - список размерности oldAriy
  arg = new ActorData(new DataListActor(), argIdx);
  arg->actor->SetDelay(0);
  actors_->append(arg);

  tArc = new ArcIG();

  tArc->dst.num = arg->num;
  tArc->dst.type = act;
  tArc->src.num = dalistSpec;
  tArc->src.type = spec;
  tArc->slot = 1; // функция
  links_->append(tArc);

  tArc = new ArcIG();

  tArc->dst.num = arg->num;
  tArc->dst.type = act;
  tArc->src.num = tOp->num;
  tArc->src.type = act;
  tArc->slot = 0; // аргумент
  links_->append(tArc);

  // добавляю юзел интерпретации аргумента новой функцией
  ActorData* interOp = new ActorData(new InterpActor(
          MemLocation(ext, -1), MemLocation(ext, -1)), actors_->size());
  interOp->actor->SetDelay(0);
  actors_->append(interOp);

  tArc = new ArcIG();

  tArc->dst.num = interOp->num;
  tArc->dst.type = act;
  tArc->src.num = extIdx;
  tArc->src.type = ext;
  tArc->slot = 1; // функция
  links_->append(tArc);

  tArc = new ArcIG();
  tArc->dst.num = interOp->num;
  tArc->dst.type = act;
  tArc->src.num = argIdx;
  tArc->src.type = act;
  tArc->slot = 0; // аргумент
  links_->append(tArc);

  int i = constComputes.size() - nComputes; // устанавливаю индекс
                                            // начала константных выражений
  // добавляю узлы в список-аургумент (входящие дуги) - пока что там 1 аргумент
  int arity = 1;
  for (; i < constComputes.size(); ++i) {
    ArcIG *link = new ArcIG();
    link->slot = arity++;
    link->src.type = act;
    link->src.num = constComputes[i]->num;
    link->dst.num = argIdx;
    link->dst.type = act;
    links_->append(link);
  }

  // заменяю дугу, входящую в возвратную вершину на дугу, исходящую из interOp
  int retNum = -1; // номер возвратной вершины
  for (ActorData* actor : *actors_)
    if (actor->actor->GetActorType() == retType) {
      retNum = actor->num;
      break;
    }
  Q_ASSERT(retNum >= 0);

  foreach (ArcIG* link, *links_) {
    if (link->dst.num == retNum) {
      link->src.num = interOp->num;
      link->src.type = act;
    }
  }
}

OptIG* nsp_o_loops::cloneFun(OptIG &optig, QString name) {
  QList<OptExtRef*>* functions_ = optig.functions(),
      *cloneFun = new  QList<OptExtRef*>;
  QList<ConstData*>* consts_ = optig.consts(),
      *cloneConsts = new QList<ConstData*>;
  QList<ActorData*>* actors_ = optig.actors(),
      *cloneActors = new QList<ActorData*>;
  QList<ArcIG*>* links_ = optig.links(),
      *cloneLinks = new QList<ArcIG*>;

  foreach (OptExtRef* t, *functions_) {
    if (t->num == 0)
      cloneFun->append(new OptExtRef(new ExtRef(name), 0));
    else
      cloneFun->append(new OptExtRef(*t));
  }
  foreach (ConstData* t, *consts_)
    cloneConsts->append(new ConstData(*t));
  foreach (ActorData* t, *actors_)
    cloneActors->append(new ActorData(*t));
  foreach (ArcIG* t, *links_)
    cloneLinks->append(new ArcIG(*t));


  OptIG *clone = new OptIG();
  clone->functions(cloneFun);
  clone->consts(cloneConsts);
  clone->actors(cloneActors);
  clone->links(cloneLinks);

  return clone;
}

int nsp_o_loops::getConstComputations(OptIG &optig
                                       , QList<ActorData *> &constComputes) {
  auto actors_ = optig.actors();
  auto links_ = optig.links();

  bool fl;  // флаг устанавливает, если на итерации
            //актор был добавлен в constComputes

  int num = 0;  // количество константных выражений
  for (;;) {
    fl = false;
    for (ActorData* actor : *actors_) {
      if (actor->actor->GetActorType() == argType)
         continue; // аргумент не константный
      //-----------
      // проверяю включен ли уже узел в список constComputes
      bool isComputes = false;
      for (ActorData* t : constComputes)
        if (*t == *actor) {
          isComputes = true;
          break;
        }
      if (isComputes)
        continue;

      //-----------
      // проверяю, все ли источники данных являются константами или
      // принадлежат списку constComputes
      bool isConst = true;
      for (ArcIG *link : *links_) {
        if (link->dst.type != act)
          continue; // проброс через зад. список
        if (link->dst.num != actor->num)
          continue; // не наш актор
        if (link->src.type != act)
          continue; // что-то не изменное
        // проверяем принадлежит ли источник данных списку constComputes
        bool isSrcConstActor = false;
        for (ActorData* t : constComputes)
          if (t->num == link->src.num) {
            isSrcConstActor = true;
            break;
          }
        if (isSrcConstActor)
          continue; // принадлежит isConst
        isConst = false; // нашли неконстантантный аргумент actor
        break;
      }
      if (false == isConst)
        continue; // actor не конcтантный, включать его в constComputes не надо

      constComputes << actor;
      ++num;
      fl = true;
    }
    if (false == fl)
      break;
  }

  return num;
}

ActorData* nsp_o_loops::getRecCall(OptIG &optig) {
  auto actors_ = optig.actors();
  auto links_ = optig.links();
  for (ActorData* nodeInter : *actors_) {
    if (interpType != nodeInter->actor->GetActorType())
      continue; // узел не интерпретация

    for (auto link : *links_)
      if (link->dst.num == nodeInter->num
        && 1 == link->slot // функциональный вход
        && link->src.type == ext
        && link->src.num == 0) {
        return nodeInter;
      }
  }
  return nullptr;
}

int nsp_o_loops::getInterArgIdx(OptIG &optig, ActorData *inter) {
  auto links_ = optig.links();
  for (auto link : *links_)
    if (link->dst.num == inter->num
      && 0 == link->slot) { // вход данных
      return link->src.num;
    }
  return -1;
}

ActorData* nsp_o_loops::getActorByIdx(OptIG &optig, int num) {
  auto actors_ = optig.actors();
  for (ActorData* t : *actors_)
    if (t->num == num)
      return t;
  return nullptr;
}

int nsp_o_loops::getConstParams(OptIG &optig, QList<ActorData*> &constArgs) {
  auto consts_ = optig.consts();
  auto actors_ = optig.actors();
  auto links_ = optig.links();

  int arity = 0;  // количество аргументов,
                  // передаваемых при рекурсивном вызове
  int argInd = -1; // индекс аругмента рекурсивного вызова
  ActorData *arg = nullptr;     // аргумент рекурсивного вызова
  ActorData *recCall = nullptr; // рекурсивный вызов
  QList<int> nums; // локальные константы с номерами 0, 1, 2, 3, ...
  QList<ActorData*> args; // узлы аргументов функций
                          // пример: 1	0	:		0 loc:0
  //-----------
  // выделение локальных констант со значениями 1, 2, 3, ...
  // эти константы могут быть индексами аргументов функции
  for (int i = 1;; ++i) {
    bool fl = false;
    foreach(ConstData *t, *consts_)
      if (t->value->GetValueType() == IntValueType)
        if (static_cast<IntValue*>(t->value)->GetIntValue() == i) {
          fl = true;
          nums << t->num;
          break;
        }
    if (false == fl)
      break;
  }

  if (nullptr == (recCall = getRecCall(optig)))
    return -1;

  if (0 > (argInd = getInterArgIdx(optig, recCall)))
    return -1;

  arg = getActorByIdx(optig, argInd);
  if (nullptr == arg)
    return -1; // рекурсивным вызовом обрабатывается не актор
  if (datalistType != arg->actor->GetActorType())
    return -1; // рекурсивном вызовом обрабатывается не список данных

  //-----------
  // определяем количество аргументов функции, используемое при вызове (arity)
  // (количество дуг, входящих в arg)
  for (auto link : *links_)
    arity += link->dst.num == arg->num;

  // получаю узлы, которые используются как аргументы функции
  // пример:
  // 1	0	:		0 loc:0		pos 2 20 2 21
  // 2	0	:		0 loc:1		pos 3 22 3 23
  // 3	0	:		0 loc:2		pos 4 21 4 22
  // Результат: 1, 2, 3
  // Повторяющийся код должен быть удален
  for (int i = 0; i < arity; ++i) {
    args << nullptr;
    for (ActorData* nodeInter : *actors_) {
      if (interpType != nodeInter->actor->GetActorType())
        continue; // не интерпретация

      bool fl = false;
      for (auto link : *links_)
        if (link->dst.num == nodeInter->num
          && 1 == link->slot // функция
          && link->src.type == loc
          && link->src.num == nums[i]) { // аргумент
          fl = true;
          break;
        }
      if (false == fl)
        continue; // не интерпретация аргумента

      for (auto link : *links_)
        if (link->dst.num == nodeInter->num
          && 0 == link->slot // данные
          && link->src.type == act
          && link->src.num == 0) { // константа
          args[i] = nodeInter;
          break;
        }
      break;
    }
  }

  // получаю не изменяемые аргументы функции при рекурсивном вызове
  for (int i = 0; i < arity; ++i) {
    if (nullptr == args[i])
      continue; // аргумент не используется

    for (auto link : *links_)
      if (link->dst.num == arg->num && link->src.num == args[i]->num) {
        constArgs << args[i];
        break;
      }
  }
  return arity;
}
