#include "value.h"

//=================================================================================================
//
Value::Value() {
}
Value* Value::ParGetElement(int elementNum)
{
    if(elementNum)
        return new ErrorValue(BOUNDERROR);
    return this;
}



//=================================================================================================
// Получение текстового представления логического значения
QString BoolValue::GetAsStr() {
    QString str;
    str = value ? "true" : "false";
    return str;
}
//=================================================================================================
// Тестовый вывод логического значения
void BoolValue::TestOut(QTextStream& out) {
    out << "type is bool, value = " << GetAsStr() << endl;
}
//=================================================================================================
// Клонировщик
Value* BoolValue::Clone()
{
    return new BoolValue(value);
}



// Получение текстового представления целочисленного значения
QString IntValue::GetAsStr()
{
    QString str="";
    str = QString("%1").arg(value);
    return str;
}
// Тестовый вывод целочисленного значения
void IntValue::TestOut(QTextStream& out)
{
    out << "type is int, value = " << GetAsStr() << endl;
}
// Клонировщик
Value* IntValue::Clone()
{
    return new IntValue(value);
}



// Получение текстового представления действительного значения
QString FloatValue::GetAsStr()
{
    QString str;
    str = QString::number(value);
    return str;
}
// Тестовый вывод действительного значения
void FloatValue::TestOut(QTextStream& out)
{
    out << "type is float, value = " << GetAsStr() << endl;
}
// Клонировщик
Value* FloatValue::Clone()
{
    return new FloatValue(value);
}



// Получение текстового представления символьного значения
QString CharValue::GetAsStr()
{
    QString str = QString("\'");
    if(value == '\n') str += "\\n";
    else if(value == '\r') str += "\\r";
    else if(value == '\t') str += "\\t";
    else if(value == '\'') str += "\\\'";
    else if(value == '\"') str += "\\\"";
    else if(value == '\\') str += "\\\\";
    else str += value;
    str += QString("\'");
    return str;
}
// Тестовый вывод символа
void CharValue::TestOut(QTextStream& out)
{
    out << "type is character, value = " << GetAsStr() << endl;
}
// Клонировщик
Value* CharValue::Clone()
{
    return new CharValue(value);
}



// Получение текстового представления строки
QString StrValue::GetAsStr() {
    QString str = "\"";
    QString::const_iterator strIter;
    for (strIter = value.constBegin(); strIter != value.constEnd(); ++strIter) {
        if(*strIter == '\n') str += "\\n";
        else if(*strIter == '\r') str += "\\r";
        else if(*strIter == '\t') str += "\\t";
        else if(*strIter == '\'') str += "\\\'";
        else if(*strIter == '\"') str += "\\\"";
        else if(*strIter == '\\') str += "\\\\";
        else str += *strIter;
    }
    str += QString("\"");
    return str;

}
// Тестовый вывод строки
void StrValue::TestOut(QTextStream& out)
{
    out << "type is string, value = " << GetAsStr() << endl;
}
// Клонировщик
Value* StrValue::Clone()
{
    return new StrValue(value);
}



// Формирование текстового значения для специального обозначения
QString GetAsStr(SpecType value)
{
    QString str;
    switch(value) {
    case dotSpec:        str = ".";        break;  // - пустое или сигнальное значение
    case addSpec:        str = "+";        break;  // - плюс
    case minusSpec:      str = "-";        break;  // - минус
    case astSpec:        str = "*";        break;  // - звездочка
    case slashSpec:      str = "/";        break;  // - слэш
    case percentSpec:    str = "%";        break;  // - процент
    case dotsSpec:       str = "..";       break;  // - многоточие
    case dupSpec:        str = "dup";      break;  // - дубликаты
    case ltSpec:         str = "<";        break;  // - меньше
    case leSpec:         str = "<=";       break;  // - меньше или равно
    case eqSpec:         str = "=";        break;  // - равно
    case neSpec:         str = "!=";       break;  // - не равно
    case geSpec:         str = ">=";       break;  // - больше или равно
    case gtSpec:         str = ">";        break;  // - больше
    case vertSpec:       str = "|";        break;  // - вертикальная черта
    case questSpec:      str = "?";        break;  // - вопрос
    case sharpSpec:      str = "#";        break;  // - решетка
    case trueSpec:       str = "true";     break;  // - истина
    case falseSpec:      str = "false";    break;  // - ложь
    case typeSpec:       str = "type";     break;  // - тип
    case valSpec:        str = "value";    break;  // - значение
    case boolSpec:       str = "bool";     break;  // - булевый
    case charSpec:       str = "char";     break;  // - символьный
    case intSpec:        str = "int";      break;  // - целый
    case floatSpec:      str = "float";    break;  // - действительный
    case strSpec:        str = "string";   break;  // - строковый
    case dalistSpec:     str = "()";       break;  // - список данных
    case palistSpec:     str = "[]";       break;  // - параллельный список
    case delistSpec:     str = "{}";       break;  // - задержанный список
    case aslistSpec:     str = "<()";      break;  // - асинхронный список
    case errorSpec:      str = "error";    break;  // - ошибка
    default:             str = "Undefined spec type";
    }
    return str;
}
// Получение текстового представления специального значения
QString SpecValue::GetAsStr()
{
    return ::GetAsStr(value);
}
// Тестовый вывод специального обозначения
void SpecValue::TestOut(QTextStream& out)
{
    out << "type is spec, value = " << GetAsStr() << endl;
}
// Клонировщик
Value* SpecValue::Clone()
{
    return new SpecValue(value);
}



// Получение текстового представления задержанной константы
QString DelayValue::GetAsStr() {
    QString str;
    str = "{" + QString::number(number) + "}" + QString::number(link);
    return str;
}
// Тестовый вывод задержанной константы
void DelayValue::TestOut(QTextStream& out) {
    out << "type is delay const, value = {" << QString::number(number) << "}" << QString::number(link) << endl;
}
// Клонировщик
Value* DelayValue::Clone()
{
    return new DelayValue(number,link,EPID);
}



// Формирование текстового значения для ошибки
QString GetAsStr(ErrorType value)
{
    QString str;
    switch(value) {
    case BASEFUNCERROR: str="BASEFUNCERROR"; break;     // 0: общая ошибка
    case TYPEERROR:     str="TYPEERROR"; break;         // 1: ошибка получения типа
    case BOUNDERROR:    str="BOUNDERROR"; break;        // 2: выход за границы массива
    case INTERROR:      str="INTERROR"; break;          // 3: ошибка при выполнении целочисленной операции
    case REALERROR:     str="REALERROR"; break;         // 4: ошибка при выполнении операции с плавающей точкой
    case ZERODIVIDE:    str="ZERODIVIDE"; break;        // 5: деление на ноль
    case VALUEERROR:    str="VALUEERROR"; break;		// 6: ошибка функции value
    default:            str="Unknown error type";
    }
    return str;
}
// Получение текстового представления ошибки
QString ErrorValue::GetAsStr()
{
    return ::GetAsStr(value);
}
// Тестовый вывод ошибки
void ErrorValue::TestOut(QTextStream& out)
{
    out << "type is error, value = " << GetAsStr() << endl;
}
// Клонировщик
Value* ErrorValue::Clone()
{
    return new ErrorValue(value);
}



// Получение текстового представления внешней сущности
QString ExternValue::GetAsStr()
{
    QString str;
    str = QString::number(value);
    return str;
}
// Тестовый вывод внешней сущности
void ExternValue::TestOut(QTextStream& out)
{
    out << "type is extern value, externId = " << GetAsStr() << endl;
}
// Клонировщик
Value* ExternValue::Clone()
{
    return new ExternValue(value,EPID);
}



// Получение текстового представления заглушки
QString MockValue::GetAsStr() {
    return "mock";
}
// Тестовый вывод внешней сущности
void MockValue::TestOut(QTextStream& out) {
    out << "type is mock value" << endl;
}
// Клонировщик
Value* MockValue::Clone()
{
    return new MockValue();
}




// Добавление элемента в список
void ListValue::Append(Value *x)
{
    elements.push_back(x);
}
void ListValue::SetElement(Value *x, int index)
{
    if(elements.size()<index)
        elements.resize(index+1);
    elements[index]=x;
}



// Получение текстового представления списка данных
QString DataListValue::GetAsStr()
{
    QString str="(";
    if(elements.size())
        str+=elements[0]->GetAsStr();
    for(signed int i=1;i<elements.size();i++)
    {
        str+=",";
        str+=elements[i]->GetAsStr();
    }
    str+=")";
    return str;
}
// Тестовый вывод списка данных
void DataListValue::TestOut(QTextStream& out)
{
    out << "type is datalist, value = " << GetAsStr();
}
// Клонировщик
Value* DataListValue::Clone()
{
    DataListValue* newDataListValue=new DataListValue();
    for(int i=0;i<elements.size();i++)
        newDataListValue->Append(elements[i]->Clone());
    return newDataListValue;
}



// Получение текстового представления параллельного списка
QString ParListValue::GetAsStr()
{
    QString str="[";
    if(elements.size())
        str+=elements[0]->GetAsStr();
    for(signed int i=1;i<elements.size();i++)
    {
        str+=",";
        str+=elements[i]->GetAsStr();
    }
    str+="]";
    return str;
}
// Тестовый вывод параллельного списка
void ParListValue::TestOut(QTextStream& out)
{
    out << "type is parlist, value = " << GetAsStr() << endl;
}
void ParListValue::Append(Value *x)
{
    elements.push_back(x);
    readyIndexes.push_back(elements.size()-1);
}
void ParListValue::SetElement(Value *x, int index)
{
    if(elements.size()<=index)
        elements.resize(index+1);
    elements[index]=x;
    readyIndexes.push_back(index);
}
//возвращаем весь список
QVector<Value*>* ParListValue::GetAllList()
{
    return &elements;
}
//для константного (или просто передающегося готовым) параллельного списка
//порядок поступления элементов не важен
//посему предполагаем, что поступали они по порядку
void ParListValue::ConstantReadyFill()
{
    readyIndexes.clear();
    for(int i=0;i<elements.size();i++)
        readyIndexes.push_back(i);
}
Value* ParListValue::ParGetElement(int elementNum)
{
    if(elementNum>elements.size())
        return new ErrorValue(BOUNDERROR);
    return elements[elementNum];
}
int ParListValue::GetReadyIndex(int offset)
{
    if(offset>readyIndexes.size())
    {
        qDebug() << " Error! Offset for readyIndexes is too big!";
        return NULL;
    }
    int index=readyIndexes[offset];
    return index;
}
// Клонировщик
Value* ParListValue::Clone()
{
    ParListValue* newParListValue=new ParListValue();
    for(int i=0;i<elements.size();i++)
        newParListValue->Append(elements[i]->Clone());
    return newParListValue;
}



//конструктор асинхронного списка
AsyncListValue::AsyncListValue()
{
    valueType=AsyncListValueType;
    head=NULL;
    tail=NULL;
}
// Получение текстового представления значения
QString AsyncListValue::GetAsStr()
{
    QString rez="<-(";
    AsyncListValue *index=this;
    while(index!=NULL && index->head!=NULL)
    {
        rez+=index->GetHead()->GetAsStr()+",";
        index=index->GetTail();
    }
    rez+=")->";
    return rez;
}
// Тестовый вывод специального обозначения
void AsyncListValue::TestOut(QTextStream& out)
{
    out << "type is asynclist, value = " << GetAsStr();
}
// Как-бы клонировщик
Value* AsyncListValue::Clone()
{
    AsyncListValue* newAsyncList=new AsyncListValue;
    if(head!=NULL)
        newAsyncList->SetHead(head->Clone());
    if(tail!=NULL)
        newAsyncList->SetTail((AsyncListValue*)(tail->Clone()));
    return newAsyncList;
}
void AsyncListValue::Append(Value *newValue)
{
    if(head==NULL)
    {
        head=newValue;
        tail=new AsyncListValue();
    }
    else
        tail->Append(newValue);
}
