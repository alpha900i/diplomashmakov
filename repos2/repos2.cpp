/*==============================================================================
   File: pfg_parser.cpp

   Description: 
        Pifagor programming language parser(syntax analyzer).
        Input: array of lexema, debug information.
        Output: information graph, debug information.

   Credits: AUTHORS

   License: GNU General Public License Version 3 or later
            http://www.gnu.org/licenses/gpl.html
==============================================================================*/

#ifndef REPOS_CPP
#define REPOS_CPP

#include "repos2.h"

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#include <direct.h>
#endif

repos::repos()
{
    strcpy(repPath,"./out/db/");
}

repos::repos(char* baseName)
{
    strcpy(repPath,baseName);
}

char* repos::w2c(wchar_t *wStr)
{
    char *cStr;
    cStr=new char[100];
    sprintf(cStr,"%ls",wStr);
    return cStr;
}

////////////////
//Name formers//
////////////////
char* repos::getIgTxtName(char *funcName)
{
    char *fileName;
    fileName=basePath(funcName);
    sprintf(fileName,"%s/1.rig",basePath(funcName));
    return fileName;
}
char* repos::getCgTxtName(char *funcName)
{
    char *fileName;
    fileName=basePath(funcName);
    sprintf(fileName,"%s/1.cg",basePath(funcName));
    return fileName;
}
char* repos::getIgDotName(char *funcName)
{
    char *fileName;
    fileName=basePath(funcName);
    sprintf(fileName,"%s/1.rig.dot",basePath(funcName));
    return fileName;
}
char* repos::getCgDotName(char *funcName)
{
    char *fileName;
    fileName=basePath(funcName);
    sprintf(fileName,"%s/1.cg.dot",basePath(funcName));
    return fileName;
}
char* repos::getCodeName(char *funcName)
{
    char *fileName;
    fileName=basePath(funcName);
    sprintf(fileName,"%s/1.pfg",basePath(funcName));
    return fileName;
}
/*==============
    Preparers
==============*/
QString repos::prepIgTxt(char* funcName)
{
    char *fileName;
    fileName=getIgTxtName(funcName);
    dirPrepare(fileName);
    return QString(fileName);
}
QString repos::prepIgDot(char* funcName)
{
    char *fileName;
    fileName=getIgDotName(funcName);
    dirPrepare(fileName);
    return QString(fileName);
}
QString repos::prepCgTxt(char* funcName)
{
    char *fileName;
    fileName=getCgTxtName(funcName);
    dirPrepare(fileName);
    return QString(fileName);
}
QString repos::prepCgDot(char* funcName)
{
    char *fileName;
    fileName=getCgDotName(funcName);
    dirPrepare(fileName);
    return QString(fileName);
}

void repos::saveCode(char* fileName, Positions start, Positions end, char* funcName)
{
    char *outName;
    outName=getCodeName(funcName);
    dirPrepare(outName);

    FILE *in=fopen(fileName,"r+t");
    FILE *out=fopen(outName,"w+t");
    if(in==NULL || out==NULL) {
        fwprintf(stdout, L"Errors file opening error.\n");
        return;
    }
    fileCopy(in,out,start,end);
    fclose(in);
    fclose(out);
}
void repos::fileCopy(FILE* in, FILE *out, Positions start, Positions end)
{
    char c;
    char buf[1000];
    int row=1, col=1;
    while(row<start.GetBeginRow())
    {
        fgets(buf,1000,in);
        row++;
    }
    while(col<start.GetBeginCol())
    {
        fscanf(in,"%c",&c);
        col++;
    }

    while(true)
    {
        fscanf(in,"%c",&c);
        fprintf(out,"%c",c);
        if(c=='\n')
        {
            row++;
            col=1;
        }
        else
        {
            if(c=='\t') col+=TAB_SIZE;
            col++;
        }
        if((row==end.GetBeginRow() && col>=end.GetBeginCol())||row>end.GetBeginRow())
            break;
    }
}

/*------------------------------------------------------------------------------
    Forms path
------------------------------------------------------------------------------*/
char* repos::basePath(char* funcName)
{
    char *fileName;
    fileName=new char[100];
    sprintf(fileName,"%s%s",repPath,funcName);
    char* dotPoint=strchr(fileName+1,'.');
    while(dotPoint)
    {
        *dotPoint='/';
        dotPoint=strchr(fileName+1,'.');
    }
    return fileName;
}
/*------------------------------------------------------------------------------
    Create all needed folders for new file
------------------------------------------------------------------------------*/
void repos::dirPrepare(char* fileName)
{
    char* dotPoint=strchr(fileName+2,'/');
    char curName[100];
    while(dotPoint)
    {
        strcpy(curName,fileName);
        curName[dotPoint-fileName]='\0';
        fileName[dotPoint-fileName]='/';

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
        mkdir(curName);
#else
        mkdir(curName,0777);
#endif

        dotPoint=strchr(dotPoint+1,'/');
    }
}

#endif



