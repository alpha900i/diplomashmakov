/*==============================================================================
	File: repos.h

	Description: 

	Credits: 

	License: GNU General Public License Version 3 or later
			 http://www.gnu.org/licenses/gpl.html
==============================================================================*/

#ifndef REPOS_H
#define REPOS_H

#include <iostream>
#include <string>
#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#else
  #include <sys/stat.h>
  #include <sys/types.h>
#endif


#include "../rig/rig.h"
#include "../trans2/pfg_scanner2.h"

using namespace std;


/*------------------------------------------------------------------------------
  Class: repos

------------------------------------------------------------------------------*/
class repos: public QObject
{
private:
	char repPath[1000];
public:
	repos();
	repos(char *baseDir);

	char* w2c(wchar_t *wStr);
	char* basePath(char* funcName);
	void dirPrepare(char* filename);

	char* getIgTxtName(char* funcName);
	char* getCgTxtName(char* funcName);
	char* getIgDotName(char* funcName);
	char* getCgDotName(char* funcName);	
    char* getCodeName(char *funcName);
	
    QString prepIgTxt(char* funcName);
    QString prepIgDot(char* funcName);
    QString prepCgTxt(char* funcName);
    QString prepCgDot(char* funcName);

    void saveCode(char* fileName, Positions start, Positions end, char* funcName);
    void fileCopy(FILE* in, FILE *out, Positions start, Positions end);

    /*void saveIgBin(igraph *ig);
	void saveIgTxt(igraph *ig);
	void saveIgDot(igraph *ig);
	void saveCgBin(cgraph *cg, char* funcName);
	void saveCgTxt(cgraph *cg, char* funcName);
    void saveCgDot(cgraph *cg, char* funcName);

    void saveCode(char* fileName, position start, position end, char* funcName);
    void fileCopy(FILE* in, FILE *out, position start, position end);

	igraph* loadIgBin(char* funcName);
    cgraph* loadCgBin(char* funcName);*/
};

#endif

