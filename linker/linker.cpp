#include <iostream>
using namespace std;

#include "../repos2/repos2.h"
#include "../rigparser/rigparser.h"
#include "../cgparser/cgparser.h"
#include "linker.h"

/*/////////////
//Конструктор//
/////////////*/
Linker::Linker()
{
    rigsFound.clear();
    cgsFound.clear();
}
/*////////////////////////////
//Главная функция компоновки//
////////////////////////////*/
int Linker::Link(char* funcName)
{
    repos rep;
    Rig *rig;
    Cg *cg;
    RigParser *rigParser;
    CgParser *cgParser;
    int curPos=-1;
    int childPos;

    rig=new Rig;
    cg=new Cg;

    //Компоновщик уже обработал эту функцию?
    for(int i=0;i<rigsFound.size();i++)
        if(rigsFound[i]->GetName()==QString(funcName))
            return i;

    //Считываем РИГ
    QString rigFileName=QString(rep.getIgTxtName(funcName));
    rigParser=new RigParser(rig,rigFileName);
    if(!(rigParser->Start()))
    {
        qDebug() << "Link error during proccesing RIG of function "+QString(funcName)+". Check file "+rigFileName;
        return LINK_ERROR;
    }
    curPos=rigsFound.size();
    rigsFound.append(rig);

    //Считываем УГ
    QString cgFileName=QString(rep.getCgTxtName(funcName));
    cgParser=new CgParser(cg,cgFileName);
    if(!(cgParser->Start()))
    {
        qDebug() << "Link error during proccesing CG of function "+QString(funcName)+". Check file "+cgFileName;
        return LINK_ERROR;
    }
    cgsFound.append(cg);

    QVector<ExtRef*> curExtRefVect=*(rig->GetExtRefList());
    for(int i=0;i<curExtRefVect.size();i++)
    {
        //Проделываем компоновку для всех внешних ссылок
        childPos=Link(curExtRefVect[i]->GetName()->toUtf8().data());
        if(childPos==LINK_ERROR)
            return LINK_ERROR;
        //Формируем карту внешних ссылок
        AddMapNote(curPos,i,childPos);
    }

    return curPos;
}
/*//////////////////////////////////////////////
//Тестовая распечатка содержимого компоновщика//
//////////////////////////////////////////////*/
void Linker::PrintAll()
{
    //Печать РИГ
    for(int i=0;i<rigsFound.size();i++)
        rigsFound[i]->SaveText(QString("./out/link/%1.rig").arg(i));
    //Печать УГ
    for(int i=0;i<cgsFound.size();i++)
        cgsFound[i]->SaveText(QString("./out/link/%1.cg").arg(i));
    //Печать карты внешних ссылок
    for(int i=0;i<mapNotes.size();i++)
        cout << "Rig #"<< mapNotes[i].rigNum << "\'s ext#" << mapNotes[i].extNum << " is "  << mapNotes[i].mapPos << endl;
}
/*/////////////////////////////////////
//Новая запись в карту внешних ссылок//
/////////////////////////////////////*/
void Linker::AddMapNote(int rigNum, int extNum, int mapPos)
{
    mapNote curNote;
    curNote.rigNum=rigNum;
    curNote.extNum=extNum;
    curNote.mapPos=mapPos;
    mapNotes.append(curNote);
}
/////////////////////////////
// поиск конкретной записи //
/////////////////////////////
int Linker::GetMapNote(int rigNum, int extNum)  // TODO L1 сделать нормальную мапу
{
    for(int i=0;i<mapNotes.size();i++)
        if(mapNotes[i].rigNum==rigNum && mapNotes[i].extNum==extNum)
            return mapNotes[i].mapPos;
    return -1;
}
