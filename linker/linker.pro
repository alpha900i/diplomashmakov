TEMPLATE = app
CONFIG   += console

OBJECTS_DIR = ../../_build/obj
DESTDIR  = ../../bin
#MAKEFILE = make_linker

TARGET = link

HEADERS += linker.h \
    ../repos2/repos2.h \
    ../cg/automat.h \
    ../cg/csignal.h \
    ../cg/ctrlnode.h \
    ../cg/dynlink.h \
    ../cg/cg.h \
    ../rigparser/rigparser.h \
    ../cgparser/cgparser.h \
    ../rig/rig.h \
    ../rig/extref.h \
    ../rig/actor.h \
    ../rig/value.h

SOURCES += linker.cpp \
    link.cpp \
    ../repos2/repos2.cpp \
    ../rigparser/rigparser.cpp \
    ../cgparser/cgparser.cpp \
    ../cg/automat.cpp \
    ../cg/csignal.cpp \
    ../cg/ctrlnode.cpp \
    ../cg/dynlink.cpp \
    ../cg/cg.cpp \
    ../rig/rig.cpp \
    ../rig/extref.cpp \
    ../rig/actor.cpp \
    ../rig/value.cpp

