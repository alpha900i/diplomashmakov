#include <QtTest/QtTest>
#include <fstream>
#include <../inter2/alu.h>

using namespace std;
Q_DECLARE_METATYPE(Value*)

class GeneralTester
{
public:
    void makeHead();
    void runTest();
};
void GeneralTester::makeHead()
{
    QTest::addColumn<QString>("funcName");
    QTest::addColumn<Value*>("argument");
    QTest::addColumn<QString>("answer");
}

//вызываем подготовленные тесты
void GeneralTester::runTest()
{
    QFETCH(QString, funcName);
    QFETCH(Value*,argument);
    QFETCH(QString, answer);

    CentralManager *cm;
    cm=new CentralManager();
    cm->Init((char*)qPrintable(funcName),argument);
    cm->Start();

    QCOMPARE(cm->GetFinalRez()->GetAsStr(),answer);
}

class QTestInter2: public QObject
{
    Q_OBJECT
public:
private:
    GeneralTester generalTester;
private slots:
    void initTestCase();
    void cleanupTestCase();

    //void test_data();
    void testInter_data();
    void testInter();
    void testParList_data();
    void testParList();
    void testExtern_data();
    void testExtern();
    void testExternDelay_data();
    void testExternDelay();

    void testSpecPass_data();
    void testSpecPass();
    void testExternPass_data();
    void testExternPass();
    void testAsyncList_data();
    void testAsyncList();

    void testFact_data();
    void testFact();
    void testSort_data();
    void testSort();

    void testErrors_data();
    void testErrors();


};

//предварительная подготовка
//прекомпиляция тестовых pfg, получение РИГ и УГ
//TODO - меньше слов. Пусть ограничится "все хорошо" или "ошибка при трансляции того-то"
void QTestInter2::initTestCase()
{
    system("trans2 -a ..\\src_pif\\qtTests\\extern.pfg");
    system("trans2 -a ..\\src_pif\\qtTests\\inter.pfg");
    system("trans2 -a ..\\src_pif\\qtTests\\parlist.pfg");
    system("trans2 -a ..\\src_pif\\qtTests\\delaylist.pfg");
    system("trans2 -a ..\\src_pif\\qtTests\\specPass.pfg");
    system("trans2 -a ..\\src_pif\\qtTests\\externPass.pfg");
    system("trans2 -a ..\\src_pif\\qtTests\\asynclist.pfg");
    system("trans2 -a ..\\src_pif\\qtTests\\RL_fact.pfg");
    system("trans2 -a ..\\src_pif\\qtTests\\RL_sort.pfg");

    system("trans2 -a ..\\src_pif\\qtTests\\errors.pfg");

    system("cgen2 -na qtTests.extern1 2>log2.txt");
    system("cgen2 -na qtTests.extern2 2>log2.txt");
    system("cgen2 -na qtTests.externMain1 2>log2.txt");
    system("cgen2 -na qtTests.externMain2 2>log2.txt");

    system("cgen2 -na qtTests.inter1 2>log2.txt");
    system("cgen2 -na qtTests.inter2 2>log2.txt");
    system("cgen2 -na qtTests.inter3 2>log2.txt");

    system("cgen2 -na qtTests.parlist1 2>log2.txt");
    system("cgen2 -na qtTests.parlist2 2>log2.txt");
    system("cgen2 -na qtTests.parlist3 2>log2.txt");

    system("cgen2 -na qtTests.delayOpener 2>log2.txt");
    system("cgen2 -na qtTests.delayReturner 2>log2.txt");
    system("cgen2 -na qtTests.delayMain1 2>log2.txt");
    system("cgen2 -na qtTests.delayMain2 2>log2.txt");

    system("cgen2 -na qtTests.passSpec 2>log2.txt");
    system("cgen2 -na qtTests.openSpec 2>log2.txt");

    system("cgen2 -na qtTests.passExtern 2>log2.txt");
    system("cgen2 -na qtTests.openExtern 2>log2.txt");
    system("cgen2 -na qtTests.summer 2>log2.txt");
    system("cgen2 -na qtTests.multer 2>log2.txt");

    system("cgen2 -na qtTests.asOpenerMain 2>log2.txt");
    system("cgen2 -na qtTests.asSummerMain 2>log2.txt");
    system("cgen2 -na qtTests.asSummer 2>log2.txt");
    system("cgen2 -na qtTests.asOpener 2>log2.txt");

    system("cgen2 -na qtTests.fact 2>log2.txt");

    system("cgen2 -na qtTests.sort.hoar.getsort 2>log2.txt");
    system("cgen2 -na qtTests.sort.hoar.getmind 2>log2.txt");
    system("cgen2 -na qtTests.sort.hoar.mind 2>log2.txt");

    system("cgen2 -na qtTests.errors.error1 2>log2.txt");
    system("cgen2 -na qtTests.errors.error2 2>log2.txt");
}
//зачистка хвостов
//пока пусто
void QTestInter2::cleanupTestCase()
{

}

//тестовые примеры на интерпретацию
void QTestInter2::testInter_data()
{
    generalTester.makeHead();
    IntValue* i1=new IntValue(1);
    IntValue* i10=new IntValue(10);
    IntValue* i212=new IntValue(212);

    QTest::newRow("1+2") << "qtTests.inter1" << (Value*)i1 << "3";
    QTest::newRow("X*2 (1)") << "qtTests.inter2" << (Value*)i1 << "2";
    QTest::newRow("X*2 (10)") << "qtTests.inter2" << (Value*)i10 << "20";
    QTest::newRow("X*2 (212)") << "qtTests.inter2" << (Value*)i212 << "424";

    DataListValue* dl1=new DataListValue();
    dl1->Append(new IntValue(10));
    dl1->Append(new IntValue(5));
    QTest::newRow("X:(+,-,*,/);X=(10,5)") << "qtTests.inter3" << (Value*)dl1 << "(15,5,50,2)";
    DataListValue* dl2=new DataListValue();
    dl2->Append(new IntValue(20));
    dl2->Append(new IntValue(2));
    QTest::newRow("X:(+,-,*,/);X=(20,2)") << "qtTests.inter3" << (Value*)dl2 << "(22,18,40,10)";
}
void QTestInter2::testInter()
{
    generalTester.runTest();
}



//тестовые примеры на параллельные списки
void QTestInter2::testParList_data()
{
    generalTester.makeHead();

    IntValue* i1=new IntValue(1);
    QTest::newRow("(10,20):[+,-,*,/];") << "qtTests.parlist1" << (Value*)i1 << "[30,-10,200,0.5]";
    DataListValue* dl1=new DataListValue();
    dl1->Append(new IntValue(15));
    dl1->Append(new IntValue(3));
    QTest::newRow("X:[+,-,*,/];X=(15,3)") << "qtTests.parlist2" << (Value*)dl1 << "[18,12,45,5]";
    DataListValue* dl2=new DataListValue();
    dl2->Append(new IntValue(16));
    dl2->Append(new IntValue(8));
    QTest::newRow("X:[+,-,*,/];X=(16,8)") << "qtTests.parlist2" << (Value*)dl2 << "[24,8,128,2]";
    QTest::newRow("[(10,20),(30,40),(50,60)]:[+,-,*,/]") << "qtTests.parlist3" << (Value*)i1
                                                         << "[30,70,110,-10,-10,-10,200,1200,3000,0.5,0.75,0.833333]";
}
void QTestInter2::testParList()
{
    generalTester.runTest();
}



//тестовые примеры на внешние функции
void QTestInter2::testExtern_data()
{
    generalTester.makeHead();

    IntValue* i1=new IntValue(10);
    QTest::newRow("extern call;2*X+5;X=10") << "qtTests.externMain1" << (Value*)i1 << "25";
    DataListValue* dl1=new DataListValue();
    dl1->Append(new IntValue(15));
    dl1->Append(new IntValue(3));
    QTest::newRow("extern call;2*X1*2*X2+15;X=(15,3)") << "qtTests.externMain2" << (Value*)dl1 << "195";
}
void QTestInter2::testExtern()
{
    generalTester.runTest();
}



//тестовые примеры на передачу и возврат задержанных списков
void QTestInter2::testExternDelay_data()
{
    generalTester.makeHead();

    DataListValue* dl1=new DataListValue();
    dl1->Append(new IntValue(10));
    dl1->Append(new IntValue(1));
    QTest::newRow("qtTests.delayMain1(10,1)=2*10=20") << "qtTests.delayMain1" << (Value*)dl1 << "20";

    DataListValue* dl2=new DataListValue();
    dl2->Append(new IntValue(20));
    dl2->Append(new IntValue(2));
    QTest::newRow("qtTests.delayMain1(20,2)=20+3=23") << "qtTests.delayMain1" << (Value*)dl2 << "23";

    DataListValue* dl3=new DataListValue();
    dl3->Append(new IntValue(30));
    dl3->Append(new IntValue(1));
    QTest::newRow("qtTests.delayMain2(30,1)=3*30=90") << "qtTests.delayMain2" << (Value*)dl3 << "90";

    DataListValue* dl4=new DataListValue();
    dl4->Append(new IntValue(40));
    dl4->Append(new IntValue(2));
    QTest::newRow("qtTests.delayMain2(40,2)=40+5=45") << "qtTests.delayMain2" << (Value*)dl4 << "45";
}
void QTestInter2::testExternDelay()
{
    generalTester.runTest();
}



//проверка передачи спек-операции в другую функцию
void QTestInter2::testSpecPass_data()
{
    generalTester.makeHead();

    DataListValue* dl1=new DataListValue();
    dl1->Append(new IntValue(1));
    dl1->Append(new IntValue(2));
    QTest::newRow("qtTests.passSpec(1,2)=1+2=3") << "qtTests.passSpec" << (Value*)dl1 << "3";

    DataListValue* dl2=new DataListValue();
    dl2->Append(new IntValue(10));
    dl2->Append(new IntValue(20));
    QTest::newRow("qtTests.passSpec(10,20)=10+20=30") << "qtTests.passSpec" << (Value*)dl2 << "30";
}
void QTestInter2::testSpecPass()
{
    generalTester.runTest();
}



//проверка передачи экстерн-объекта в другую функцию
void QTestInter2::testExternPass_data()
{
    generalTester.makeHead();

    DataListValue* dl1=new DataListValue();
    dl1->Append(new IntValue(10));
    dl1->Append(new IntValue(20));
    QTest::newRow("qtTests.passExtern(10,20)=(10+20,10*20)=(30,200)") << "qtTests.passExtern" << (Value*)dl1 << "(30,200)";

    DataListValue* dl2=new DataListValue();
    dl2->Append(new IntValue(25));
    dl2->Append(new IntValue(40));
    QTest::newRow("qtTests.passExtern(25,40)=(25+40,25*40)=(65,1000)") << "qtTests.passExtern" << (Value*)dl2 << "(65,1000)";
}
void QTestInter2::testExternPass()
{
    generalTester.runTest();
}



void QTestInter2::testAsyncList_data()
{
    generalTester.makeHead();

    IntValue* i1=new IntValue(1);
    QTest::newRow("qtTests.asOpenerMain=(1,(3,(2,(5,(4,\"END\"))))))") << "qtTests.asOpenerMain" << (Value*)i1 << "(1,(3,(2,(5,(4,\"END\")))))";
    QTest::newRow("qtTests.asSummerMain=15") << "qtTests.asSummerMain" << (Value*)i1 << "15";
}
void QTestInter2::testAsyncList()
{
    generalTester.runTest();
}



//RL-тест: расчет факториала
void QTestInter2::testFact_data()
{
    generalTester.makeHead();

    QString str="X!;X=";
    int fact=1;
    IntValue* x;
    for(int i=1;i<10;i++)
    {
        x=new IntValue(i);
        fact*=i;
        QTest::newRow(qPrintable(str+QString::number(i))) << "qtTests.fact" << (Value*)x << QString::number(fact);
    }
}
void QTestInter2::testFact()
{
    generalTester.runTest();
}



//RL-тест: сортировка Хоара
void QTestInter2::testSort_data()
{
    generalTester.makeHead();

    DataListValue* dl1=new DataListValue();
    dl1->Append(new IntValue(1));
    dl1->Append(new IntValue(2));
    dl1->Append(new IntValue(3));
    dl1->Append(new IntValue(4));
    dl1->Append(new IntValue(5));
    dl1->Append(new IntValue(4));
    dl1->Append(new IntValue(3));
    dl1->Append(new IntValue(2));
    dl1->Append(new IntValue(1));
    QTest::newRow("sort") << "qtTests.sort.hoar.getsort" << (Value*)dl1 <<"(1,1,2,2,3,3,4,4,5)";
}
void QTestInter2::testSort()
{
    generalTester.runTest();
}



void QTestInter2::testErrors_data()
{
    generalTester.makeHead();

    IntValue* i1=new IntValue(1);
    QTest::newRow("qtTests.errors.error1=[1,2,3,4,5]") << "qtTests.errors.error1" << (Value*)i1 << "[1,2,3,4,5]";
    QTest::newRow("qtTests.errors.error2=[1,2,3,4,5]") << "qtTests.errors.error2" << (Value*)i1 << "[1,2,3,4,5]";
}
void QTestInter2::testErrors()
{
    generalTester.runTest();
}


QTEST_MAIN(QTestInter2)

#include "qtTestsInter2.moc"
